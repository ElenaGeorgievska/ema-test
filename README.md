
# [![CodeWell](https://codewell.is/tools/images/codewell-logo.svg  "CodeWell")](https://codewell.is) The Platform Backend  
  
This is the server that is used for **receiving the users' requests**, **handling them** and for **calling the AI Server for classifying**.  

# Table of Contents
- [Use the Server as a Service](#use-the-server-as-a-service)
- [Development Setup](#development-setup)
- [Deployments](#deployments)
  
# Use the Server as a Service  
  
This is the recommended setup for development purposes when you're **NOT** working on the **server** itself, but need it as a service.  

1. Install **Docker** for your environment  
	- For [**MacOS**](https://docs.docker.com/docker-for-mac/install/)
	- For [**Linux**](https://docs.docker.com/engine/install/)
	- For [**Windows**](https://docs.docker.com/docker-for-windows/install/)
		- *or finally do something nice for yourself and start using Linux or MacOS*
2. Install [**docker-compose**](https://docs.docker.com/compose/install/#install-compose)
	> if you are on MacOS, probably you won't need to install this explicitly
4. Make sure your Docker engine is running
  
You can use `docker-compose` to start and stop the server container 
  
### Starting up (first time)  
  
1. Navigate to the root of the project  
2. Create an `.env` file based on the `env-example.txt` file.  
	- You can find the `env-example.txt` file in root of the project  
	- Modify the [environment variables](./docs/ENV_README.md) that have no value, or change any of the others  
	- Do not, I repeat, do not use `"" or ''` for the values.  
3. `docker-compose build`  
		- This will build the image and configure it with the default environment variables  
4. Then you can start it up  
	- `docker-compose up`  
		- This will start the server.


### Starting up (second and other times)  
  
1. Navigate to the root of project  
2. `docker-compose up`  
  
### Changing Env Variables  
  
Environment variables are set when the containers are created. Running `docker-compose up` will not create the containers if they already exist  
  
To change the environment variables you will need to  
  
1. `docker-compose down`  
1. Edit the `.env` file  
1. `docker-compose up`  
  
### Accessing the server  
  
The server after being started with compose, runs in a container, and exposes the `5000 port` to localhost.
    
## Building the Server docker image locally  
  
`docker build -t <image_name_and_tag> .`  
  
The image **name** and **tag** should reflect the name of the image and version.
By default, not specifying a tag will result to the implicit `latest` tag  
  
`docker build -t server .`

```
$ docker image list  
REPOSITORY         TAG         IMAGE ID            CREATED             SIZE  
server           latest      1fa3a85e914d        About an hour ago   615MB  
```

`docker build -t server:0.1.1`

```
$ docker image list  
REPOSITORY         TAG         IMAGE ID            CREATED             SIZE  
server           latest      1fa3a85e914d        About an hour ago   613MB  
server           0.1.1       1fa3a85e914d        About a minute ago  613MB  
```
    
# Development Setup 

- Navigate to the root of the project
- Run `npm install` to install the dependencies
- Run `npm start` to start the server
	- This will start the project with the package [nodemon](https://www.npmjs.com/package/nodemon) so, any changes to the source code will be automatically detected and the server restarted.
    
# Deployments  
  
## Deploy on Heroku  
  
You can check out how you can deploy your project on **Heroku** [here](./docs/HEROKU_README.md)  
  


