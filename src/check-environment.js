const log = require('./log/winston').log(__filename);

module.exports = function () {
    const requiredEnvVariables = [{
        name: 'CLASSIFY_SERVER_URL'
    }, {
        name: 'DATABASE_URL',
        customErrorMessage: `look for it on firebase`
    }, {
        name: 'SERVICE_ACCOUNT',
        customErrorMessage: `look for it on firebase`
    }, {
        name: 'TYPING_SPEED_AVG',
        customErrorMessage: `hint: 20`
    }, {
        name: 'TYPING_MULTIPLIER',
        customErrorMessage: `hint: 1000`
    }, {
        name: 'MAX_TYPING_SPEED',
        customErrorMessage: 'hint: 2500'
    }, {
        name: 'MIN_TYPING_SPEED',
        customErrorMessage: 'hint: 200'
    }, {
        name: 'ONBOARDING_STATE',
        customErrorMessage: `onboarding-start`
    }, {
        name: 'EXIT_STATE_IF_NO_ANSWER_DELAY_IN_MS',
        customErrorMessage: `hint: 60000`
    }, {
        name: 'MESSAGE_EVENT_DEBOUNCE_MS',
        customErrorMessage: 'hint: 2500'
    }];


    /** required only when you use the messenger platform **/
    const requiredForMessengerPlatform = [{
        name: 'MESSENGER_PAGE_ID',
        customErrorMessage: `hint: used for the handover protocol`
    }, {
        name: 'MESSENGER_APP_ID'
    }, {
        name: 'MESSENGER_PAGE_ACCESS_TOKEN',
        customErrorMessage: `look for it on developers.facebook.com`
    }, {
        name: 'MESSENGER_VERIFY_TOKEN'
    }];

    const warnings = [{
        name: 'MESSAGE_FEED_LIMIT',
        defaultValue: 20
    }, {
        name: 'RETURN_TO_PREVIOUS_STATE_SCENARIO',
        description: 'This is used for the states history implementation...',
    }];

    for (const variable of requiredEnvVariables) {
        if (!process.env[variable.name]) {
            let errorMessage = `missing env variable ${variable.name}`;
            if (variable.customErrorMessage) {
                errorMessage += ` (${variable.customErrorMessage})`;
            }
            throw Error(errorMessage);
        }
    }

    for (const variable of requiredForMessengerPlatform) {
        if (!process.env[variable.name]) {
            let errorMessage = `missing env variable ${variable.name}`;
            if (variable.customErrorMessage) {
                errorMessage += ` (${variable.customErrorMessage})`;
            }
            throw Error(errorMessage);
        }
    }

    for (const variable of warnings) {
        if (!process.env[variable.name]) {
            let warningMessage = `missing env variable ${variable.name}`;
            log.warn(warningMessage);
            if (variable.description) {
                log.warn(variable.description);
            }
            if (variable.defaultValue) {
                log.warn(`setting default value [${variable.defaultValue}]`);
                process.env[variable.name] = variable.defaultValue;
            }
        }
    }
};
