require('dotenv').config();

const log = require('./log/winston').log(__filename);

const startServer = require('./start-server');
const checkEnvironment = require('./check-environment');
const initContext = require('./init-context');
const initPlatformApi = require('./platform/api/init-api');
const initProjectApi = require('./project_name/api/init-api');
checkEnvironment();

initContext().then(components => {
    const platformApi = initPlatformApi(components);
    const projectApi = initProjectApi(components);
    startServer(components, platformApi, projectApi);
}).catch(err => log.error(`error while initializing the context: ${JSON.stringify(err)}`));
