const {gql} = require("apollo-server-express");

// An empty schema defining the Subscription, Query, Mutation
// types so they can be extended in the Platform and the Project
const schema = gql`
    type Subscription {
        _empty: String
    }

    type Query {
        _empty: String
    }

    type Mutation {
        _empty: String
    }
`;
module.exports = schema;
