const {ApolloServer} = require('apollo-server-express');
const http = require('http');
const baseSchema = require('./schema');
const {merge} = require('lodash');

const getSubscriptionOptions = (components) => {
    /*
    path: string;
    keepAlive?: number;
    onConnect?: (
      connectionParams: Object,
      websocket: WebSocket,
      context: ConnectionContext,
    ) => any;
    onDisconnect?: (websocket: WebSocket, context: ConnectionContext)
     */
    return {
        onConnect: (connectionParams, webSocket, context) => {
            // example authentication on socket
            if (connectionParams.authToken) {
                //validate token
                // find out which user is connecting based on the token
                // set it in the context
            }
        }
    }
};

/**
 * @param expressServer
 * @param components
 *   initialized components for both the platform and the project
 * @param platformGraphqlApi
 *   a {schema, resolvers} object containing the GraphQL schema defined from the platform
 *   and the resolvers for that schema
 * @param projectGraphqlApi
 *   a {schema, resolvers} object containing the GraphQL schema defined for other parts
 *   of the project and the resolvers for that schema
 */
const initApolloServer = (expressServer, components, platformGraphqlApi, projectGraphqlApi) => {
    const typeDefs = [baseSchema, platformGraphqlApi.schema];
    if (projectGraphqlApi.schema) {
        typeDefs.push(projectGraphqlApi.schema);
    }
    let resolvers = platformGraphqlApi.resolvers;
    if (projectGraphqlApi.resolvers) {
        resolvers = merge(resolvers, projectGraphqlApi.resolvers)
    }
    const apolloConfig = {
        typeDefs,
        resolvers,
        subscriptions: getSubscriptionOptions(components)
    };
    const server = new ApolloServer(apolloConfig);
    server.applyMiddleware({app: expressServer, path: '/graphql'});
    const httpServer = http.createServer(expressServer);
    server.installSubscriptionHandlers(httpServer);
    return httpServer;
};

module.exports = initApolloServer;
