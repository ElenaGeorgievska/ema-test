const _ = require('lodash');

const getChoiceByValue = function (choices, value) {
    return _.find(choices, choice => choice.value === value);
};

const getFirstChoice = function (choices) {
    if (choices && choices.length > 0) {
        return choices[0];
    }
};

const getRandomChoice = function (choices) {
    if (choices && choices.length > 0) {
        return choices[Math.floor(Math.random() * choices.length)];
    }
};

const getRandomMessage = function (messages) {
    return messages[Math.floor(Math.random() * messages.length)];
};

const getPlainTextFromMessage = function (messages) {
    if (!messages) return;

    const randomMessage = getRandomMessage(messages);
    return randomMessage[0];
};

const getFormattedMessages = function (messages, values) {
    if (!messages) return;
    if (!values) return messages;

    const newMessages = [];

    for (const message of messages) {
        const newMessageParts = [];
        for (const messagePart of message) {
            let newMessagePart = messagePart;
            Object.keys(values).forEach(key => {
                newMessagePart = newMessagePart.split(`{{${key}}}`).join(values[key]);
            });
            newMessageParts.push(newMessagePart);
        }
        newMessages.push(newMessageParts);
    }

    return newMessages;
};

const getTypingSpeed = function (characters) {
    let length = 0;
    if(characters){
        length = characters.length || 0;
    }
    let typingDelay = (length / process.env.TYPING_SPEED_AVG) * process.env.TYPING_MULTIPLIER;
    if (typingDelay > process.env.MAX_TYPING_SPEED) {
        typingDelay = process.env.MAX_TYPING_SPEED;
    } else if (typingDelay < process.env.MIN_TYPING_SPEED) {
        typingDelay = process.env.MIN_TYPING_SPEED;
    }
    return typingDelay;
}

module.exports = {
    getChoiceByValue, getFirstChoice, getRandomChoice, getPlainTextFromMessage, getFormattedMessages, getTypingSpeed
};
