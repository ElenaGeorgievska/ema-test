const State = require('../../../state');

const {EventResult} = require('../../../../../models/event-result');
const {Message} = require('../../../../../models/message');

class ReturnToStateFromHistory extends State {
    constructor(stateData, {userService, stateService, classifyApiService}, stateCode) {
        super(stateData, {classifyApiService});

        this.userService = userService;
        this.stateService = stateService;
        this.stateCode = stateCode;
    }

    async onStart(event) {
        let messageObj;
        if (this.messages) {
            const randomMessage = this.messages[Math.floor(Math.random() * this.messages.length)];
            messageObj = Message.TextMessages(randomMessage, this.choices);
        }
        const {userId, chatId} = event;
        const previousStateData = await this.userService.getPreviousStateData({userId, chatId});
        await this.userService.removePreviousStateData({userId, chatId});

        let nextStateId = previousStateData.stateId;

        if (this.stateCode) {
            nextStateId = previousStateData[this.stateCode];
        }

        if (nextStateId) {

            const lang = await this.userService.getUserLanguage(userId);
            const nextStateData = await this.stateService.getStateData(nextStateId, lang);

            if (nextStateData) {
                if (nextStateData.returnMessages) {
                    const randomMessage = nextStateData.returnMessages[Math.floor(Math.random() * nextStateData.returnMessages.length)];
                    messageObj = Message.TextMessages(randomMessage, nextStateData.choices);
                } else if (nextStateData.messages) {
                    const randomMessage = nextStateData.messages[Math.floor(Math.random() * nextStateData.messages.length)];
                    messageObj = Message.TextMessages(randomMessage, nextStateData.choices);
                }
            }

            return EventResult.SwitchState(nextStateId, 0, messageObj);
        }

        return undefined;
    }
}

module.exports = ReturnToStateFromHistory;
