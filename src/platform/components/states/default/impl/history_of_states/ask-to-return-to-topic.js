const _ = require('lodash');

const State = require('../../../state');

const {EventResult} = require('../../../../../models/event-result');
const {Message} = require('../../../../../models/message');
const {getChoiceByValue} = require('../../../../../../platform/components/states/common-functions');

class AskToReturnToTopic extends State {
    constructor(stateData, {classifyApiService, userService}) {
        super(stateData, {classifyApiService});
        this.userService = userService;
    }

    async onStart(event) {
        const {userId, chatId} = event;
        const previousState = await this.userService.getPreviousStateData({userId, chatId});
        if (!previousState || !previousState.topic || !this.messages) {
            const choice = getChoiceByValue(this.choices, 'no_topic');
            return EventResult.SwitchState(choice.link);
        }

        let messages = this.messages[Math.floor(Math.random() * this.messages.length)];
        let newMessages = [];

        for (let message of messages) {
            console.log(`message: ${message}`);
            if (message.includes('{{topic}}')) {
                message = message.replace('{{topic}}', previousState.topic);
            }
            newMessages.push(message);
        }

        const messageObj = Message.TextMessages(newMessages, this.choices);
        return EventResult.Wait(messageObj);
    }
}

module.exports = AskToReturnToTopic;
