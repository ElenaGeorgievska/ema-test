const State = require('../../../state');

const {EventResult} = require('../../../../../models/event-result');
const {Message} = require('../../../../../models/message');
const {getFirstChoice} = require('../../../common-functions');

class RemoveStateFromHistory extends State {
    constructor(stateData, {userService, classifyApiService}) {
        super(stateData, {classifyApiService});

        this.userService = userService;
    }

    async onStart(event) {
        const {userId, chatId} = event;
        await this.userService.removePreviousStateData({userId, chatId});

        let messageObj;
        if (this.messages) {
            const randomMessage = this.messages[Math.floor(Math.random() * this.messages.length)];
            messageObj = Message.TextMessages(randomMessage, this.choices);
        }

        const choice = getFirstChoice(this.choices);
        if (!choice) {
            return EventResult.Wait(messageObj);
        }

        return EventResult.SwitchState(choice.link, 0, messageObj);
    }
}

module.exports = RemoveStateFromHistory;
