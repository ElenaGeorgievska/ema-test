const State = require('../../../state');

const {EventResult} = require('../../../../../models/event-result');
const {getChoiceByValue} = require('../../../common-functions');

class SaveStateInHistory extends State {
    constructor(stateData, {userService, classifyApiService}) {
        super(stateData, {classifyApiService});

        this.userService = userService;
    }

    async onStart(event) {
        const {userId, chatId} = event;

        const toContinue = getChoiceByValue(this.choices, 'continue');
        const onSuccess = getChoiceByValue(this.choices, 'on_success');
        const onFail = getChoiceByValue(this.choices, 'on_fail');
        const startScenarioChoice = getChoiceByValue(this.choices, 'start');

        const stateData = this.stateData;
        stateData.currentStateId = this.id;
        stateData.timestamp = Date.now();

        if (onSuccess) {
            stateData.onSuccess = onSuccess.link;
        }
        if (onFail) {
            stateData.onFail = onFail.link;
        }
        if (toContinue) {
            stateData.stateId = toContinue.link;
        }

        stateData.scenario.important = true;

        await this.userService.savePreviousStateData({userId, chatId}, stateData);
        return EventResult.SwitchState(startScenarioChoice.link);
    }
}

module.exports = SaveStateInHistory;
