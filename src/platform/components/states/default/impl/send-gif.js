const State = require('../../state');

const {getFirstChoice} = require('../../common-functions');
const {Message} = require('../../../../models/message');
const {UrlMessage, URL_MSG_TYPE} = require('../../../../models/url-message');
const {EventResult} = require('../../../../models/event-result');

class SendGif extends State {
    constructor(stateData, {gifsService, classifyApiService}) {
        super(stateData, {classifyApiService});

        this.gifType = stateData.gifType;
        this.gifsService = gifsService;
    }

    async onStart(event) {
        const choice = getFirstChoice(this.choices);

        const url = await this.gifsService.getGif(this.gifType);
        if (!url) {
            if (choice) {
                return EventResult.SwitchState(choice.link);
            }
        }

        const urlObj = new UrlMessage(URL_MSG_TYPE.GIF, url);
        const messageObj = Message.UrlMessage(urlObj, this.choices);
        if (choice) {
            return EventResult.SwitchState(choice.link, 0, messageObj);
        }

        return EventResult.Wait(messageObj);
    }

    async _onActionEvent(event) {
        const choice = getFirstChoice(this.choices);
        return EventResult.SwitchState(choice.link);
    }
}

module.exports = SendGif;
