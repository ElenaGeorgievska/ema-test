const _ = require('lodash');

const DefaultSwitchState = require('../../switch-state');
const {EventResult} = require('../../../../models/event-result');

const {getChoiceByValue} = require('../../common-functions');

// todo: add draw url
class EnterEmail extends DefaultSwitchState {
    constructor(stateData, {classifyApiService, userService, emailValidator}) {
        super(stateData, {classifyApiService});

        this.userService = userService;
        this.emailValidator = emailValidator;

        this.delayInMs = _.defaultTo(stateData.delay, process.env.EXIT_STATE_IF_NO_ANSWER_DELAY_IN_MS)
    }

    async _onMessageEvent(event) {
        const email = event.message;
        const userId = event.userId;
        const isValid = this.emailValidator.isValid(email);
        if (!isValid) {
            let email = this.emailValidator.getInvalidEmailFromText(event.message);
            if (!email) {
                return super._onMessageEvent(event);
            }

            const choice = getChoiceByValue(this.choices, 'not_valid');
            return EventResult.SwitchState(choice.link);
        }

        await this.userService.saveUsersEmail(userId, email);
        const choice = getChoiceByValue(this.choices, 'valid');
        return EventResult.SwitchState(choice.link);
    }
}

module.exports = EnterEmail;
