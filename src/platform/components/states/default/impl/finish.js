const State = require('../../state');

const {EventResult} = require('../../../../models/event-result');
const {Message} = require('../../../../models/message');

class FinishState extends State {
    constructor(stateData, {classifyApiService, stateService}) {
        super(stateData, {classifyApiService, stateService});
    }

    async onStart(event) {
        if (!this.messages) {
            return EventResult.ScenarioFinished();
        }

        const randomMessage = this.messages[Math.floor(Math.random() * this.messages.length)];
        const messageObj = Message.TextMessages(randomMessage, this.choices);
        return EventResult.ScenarioFinished(messageObj);
    }
}

module.exports = FinishState;
