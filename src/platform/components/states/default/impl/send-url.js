const State = require('../../state');

const {getFirstChoice, getPlainTextFromMessage} = require('../../common-functions');
const {Message} = require('../../../../models/message');
const {UrlMessage, URL_MSG_TYPE} = require('../../../../models/url-message');
const {EventResult} = require('../../../../models/event-result');

class SendUrl extends State {
    constructor(stateData, {classifyApiService}) {
        super(stateData, {classifyApiService});
    }

    async onStart(event) {
        const choice = getFirstChoice(this.choices);

        const url = getPlainTextFromMessage(this.messages);
        if (!url) {
            return EventResult.SwitchState(choice.link);
        }

        const urlObj = new UrlMessage(URL_MSG_TYPE.PLAIN_URL, url);
        const messageObj = Message.UrlMessage(urlObj, this.choices);
        return EventResult.SwitchState(choice.link, 0, messageObj);
    }

    async _onActionEvent(event) {
        const choice = getFirstChoice(this.choices);
        return EventResult.SwitchState(choice.link);
    }
}

module.exports = SendUrl;
