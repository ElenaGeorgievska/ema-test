const DefaultSwitchState = require('../../switch-state');

const {getRandomChoice} = require('../../common-functions');

class SwitchStateRandom extends DefaultSwitchState {
    constructor(stateData, {classifyApiService}) {
        super(stateData, {classifyApiService});
    }

    async onStart(event) {
        const choice = getRandomChoice(this.choices);
        return super.onStart(event, choice);
    }
}

module.exports = SwitchStateRandom;
