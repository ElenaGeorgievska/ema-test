const _ = require('lodash');

const State = require('../state-model');

const {getChoiceByValue} = require('../../common-functions');
const {EventResult} = require('../../../../models/event-result');
const {Message} = require('../../../../models/message');

class WaitForAnswer extends State {
    constructor(stateData, {classifyApiService}) {
        super(stateData, {classifyApiService});

        this.delayInMs = _.defaultTo(stateData.delayInMs, process.env.EXIT_STATE_IF_NO_ANSWER_DELAY_IN_MS)
    }

    async onStart(event) {
        const messageObj = Message.TextMessages(this.messages, this.choices);
        const choice = getChoiceByValue('no_answer');

        return EventResult.SwitchState(choice.link, this.delayInMs, messageObj);
    }
}

module.exports = WaitForAnswer;
