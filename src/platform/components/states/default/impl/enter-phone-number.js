const _ = require('lodash');

const DefaultSwitchState = require('../../switch-state');
const {EventResult} = require('../../../../models/event-result');

const {getChoiceByValue} = require('../../common-functions');

// todo: add draw url
class EnterPhoneNumber extends DefaultSwitchState {
    constructor(stateData, {classifyApiService, userService, phoneNumberValidator}) {
        super(stateData, {classifyApiService});

        this.userService = userService;
        this.phoneNumberValidator = phoneNumberValidator;
        this.delayInMs = _.defaultTo(stateData.delay, process.env.EXIT_STATE_IF_NO_ANSWER_DELAY_IN_MS)

    }

    async _onMessageEvent(event) {
        const {message, userId} = event;
        let phoneNumber = this.phoneNumberValidator.hasValidPhoneNumber(message);

        if (!phoneNumber) {
            phoneNumber = this.phoneNumberValidator.matchInvalidPhoneNumberInText(message);
            if (!phoneNumber) {
                return super._onMessageEvent(event);
            }
            const choice = getChoiceByValue(this.choices, 'not_valid');
            return EventResult.SwitchState(choice.link);
        }

        await this.userService.saveUsersPhoneNumber(userId, phoneNumber);
        const choice = getChoiceByValue(this.choices, 'valid');
        return EventResult.SwitchState(choice.link);
    }
}

module.exports = EnterPhoneNumber;
