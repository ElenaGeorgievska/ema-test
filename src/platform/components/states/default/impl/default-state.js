const State = require('../../state');

class DefaultState extends State {
    constructor(stateData, {classifyApiService}) {
        super(stateData, {classifyApiService});
    }
}

module.exports = DefaultState;
