const SwitchState = require('../switch-state');
const SwitchStateRandom = require('./impl/switch-state-random');
const SendUrl = require('./impl/send-url');
const SendGif = require('./impl/send-gif');

const EnterEmail = require('./impl/enter-email');
const EnterPhoneNumber = require('./impl/enter-phone-number');

const Finish = require('./impl/finish');

const SaveStateInHistory = require('./impl/history_of_states/save-state-in-history');
const RemoveStateFromHistory = require('./impl/history_of_states/remove-state-from-history');
const ReturnToStateFromHistory = require('./impl/history_of_states/return-to-state-from-history');
const AskToReturnToTopic = require('./impl/history_of_states/ask-to-return-to-topic');

const stateTypes = require('./state-types');

class StateProvider {
    constructor(components) {
        this.components = components;
    }

    getState(stateData) {
        switch (stateData.type) {
            case stateTypes.history_of_states.save_state_in_history:
                return new SaveStateInHistory(stateData, this.components);
            case stateTypes.history_of_states.ask_to_return_to_topic:
                return new AskToReturnToTopic(stateData, this.components);
            case stateTypes.history_of_states.return_to_previous_state:
                return new ReturnToStateFromHistory(stateData, this.components);
            case stateTypes.history_of_states.previous_state_on_fail:
                return new ReturnToStateFromHistory(stateData, this.components, 'onFail');
            case stateTypes.history_of_states.previous_state_on_success:
                return new ReturnToStateFromHistory(stateData, this.components, 'onSuccess');
            case stateTypes.history_of_states.remove_state_from_history:
                return new RemoveStateFromHistory(stateData, this.components);

            case stateTypes.switch_state_types.switch_state:
                return new SwitchState(stateData, this.components);
            case stateTypes.switch_state_types.switch_state_random:
                return new SwitchStateRandom(stateData, this.components);

            case stateTypes.enter_phone_number:
                return new EnterPhoneNumber(stateData, this.components);
            case stateTypes.enter_email:
                return new EnterEmail(stateData, this.components);

            case stateTypes.gif:
                return new SendGif(stateData, this.components);

            case stateTypes.url:
                return new SendUrl(stateData, this.components);

            case stateTypes.finish:
                return new Finish(stateData, this.components);
        }
    }
}

module.exports = StateProvider;
