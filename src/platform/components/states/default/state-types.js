module.exports = {
    switch_state_types: {
        switch_state: 'switch_state',
        switch_state_random: 'switch_state_random'
    },
    history_of_states: {
        save_state_in_history: 'save_state_in_history',
        ask_to_return_to_topic: 'ask_to_return_to_topic',
        return_to_previous_state: 'return_to_previous_state',
        previous_state_on_success: 'previous_state_on_success',
        previous_state_on_fail: 'previous_state_on_fail',
        remove_state_from_history: 'remove_state_from_history'
    },
    gif: 'gif',
    finish: 'finish',
    url: 'url',
    wait_for_answer: 'wait_for_answer',
    enter_phone_number: 'enter_phone_number',
    enter_email: 'enter_email'
};
