const State = require('./state');

const {EventResult} = require('../../models/event-result');
const {Message} = require('../../models/message');

const {getFirstChoice} = require('./common-functions');


class SwitchState extends State {
    constructor(stateData, {classifyApiService}) {
        super(stateData, {classifyApiService});

        this.delayInMs = stateData.delayInMs;
    }

    async onStart(event, choice) {
        if (!choice) {
            choice = getFirstChoice(this.choices);
        }

        if (this.messages) {
            const randomMessage = this.messages[Math.floor(Math.random() * this.messages.length)];
            const messageObj = Message.TextMessages(randomMessage, this.choices);
            return EventResult.SwitchState(choice.link, this.delayInMs, messageObj);
        }

        return EventResult.SwitchState(choice.link, this.delayInMs);
    }

    async _onActionEvent(event) {
        const choice = getFirstChoice(this.choices);
        const nextStateId = choice.link;

        return EventResult.SwitchState(nextStateId);
    }
}

module.exports = SwitchState;
