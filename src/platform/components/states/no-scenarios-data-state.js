const {CLASSIFICATION_RESULT_TYPE} = require('../../models/classification-result');
const {EventResult} = require('../../models/event-result');

class NoScenariosDataState {
    constructor({classifyApiService}) {
        this.classifyApiService = classifyApiService;
    }

    async onStart(event) {
    }

    async onEvent(event) {
        const classificationResult = await this.classify(event);
        if (classificationResult && classificationResult.type === CLASSIFICATION_RESULT_TYPE.ACTION) {
            return EventResult.SwitchState(event.link);
        }

        return classificationResult;
    }

    async classify(event) {
        const {userId, chatId, message, language} = event;
        return await this.classifyApiService.runClassifying({userId, chatId, message, language});
    }
}

module.exports = NoScenariosDataState;
