const {CLASSIFICATION_RESULT_TYPE} = require('../../models/classification-result');

const {EventResult} = require('../../models/event-result');
const {Message} = require('../../models/message');

class State {
    constructor(stateData, {classifyApiService}) {
        this.stateData = stateData;
        this.messages = stateData.messages;
        this.choices = stateData.choices;
        if (this.choices) {
            this.choices = this.choices.map(c => {
                const choice = c;
                if (!choice.descriptions) {
                    choice.descriptions = [choice.description]
                }
                return choice;
            })
        }
        this.type = stateData.type;
        this.id = stateData.id;
        this.topic = stateData.topic;
        this.scenario = stateData.scenario;
        this.classifyType = stateData.classifyType;
        this.classifyApiService = classifyApiService;
    }

    async onStart(event) {
        if (this.messages) {
            const randomMessage = this.messages[Math.floor(Math.random() * this.messages.length)];
            const messageObj = Message.TextMessages(randomMessage, this.choices);
            return EventResult.Wait(messageObj);
        }
    }

    async onEvent(event) {
        if (event.type === 'action') {
            return this._onActionEvent(event);
        }

        return this._onMessageEvent(event);
    }

    async _onActionEvent(event) {
        return EventResult.SwitchState(event.link);
    }

    async _onMessageEvent(event) {
        const classificationResult = await this.classify(event);
        if (classificationResult && classificationResult.type === CLASSIFICATION_RESULT_TYPE.ACTION) {
            return this._onActionEvent(classificationResult.event);
        }

        return classificationResult;
    }

    async classify(event) {
        const {userId, chatId, message, language} = event;
        return await this.classifyApiService.runClassifying({userId, chatId, message, language}, this.stateData);
    }
}

module.exports = State;
