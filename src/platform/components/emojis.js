const checkForEmojis = require('emoji-regex/index');
const emojiRegex = checkForEmojis();

const emojis = [':)', ':(', ':D', ';)', ':P', ':/', ':*', '^_^', '^^', '<3', 'xD', '☺'];
const emojis_with_consecutive_chars = [':))', ':((', ':DD', ';))', ':PP', '://', ':**', '<33', ':OO', 'xDD'];

const allViberEmojis = ['(heart)', '(inlove)', '(nerd)', '(like)', '(waving)', '(Q)', '(!)', '(2_hearts)',
    '(thinking)', '(heart_break)', '(yellow_heart)', '(key)', '(drum)', '(crown)', '(sun)', '(alien)', '(pizza)',
    '(donut)', '(cherry)', '(trafficlight)', '(checkmark)'];


const allEmojis = [
    '😉', '😊', '☺️', '🥰', '😍', '😌', '💋', '😘', '🍻', '🐶', '🙀', '🐱', '💙', '😜', '🛀',
    '😇', '😀', '😯', '😌', '🕶️', '👽', '🆗', '👌🏽', '🌟', '🌠', '✌️', '💞', '👐', '👏', '🙈', '🙊', '🙉',
    '❓', '☹️', '💥', '💪', '💃', '👀', '🙄', '💬', '☃️', '🐸', '☕', '👶', '✅', '🐒', '🦁', '🐘', '🐔', '🦜',
    '🐊', '🐢', '🐍', '🌷', '🌹', '🌫️', '🥂', '🎂', '🍪', '🍩', '🍨', '🍕', '🍟', '🍔', '🥐', '🥨', '🥯', '🥞', '🔊',
    '🕛🕧🕐🕜🕑🕝🕒', '🕛🕧🕐🕜🕑🕝🕒💤', '💤', '💢', '💯', '💔', '❤️', '🚗', '🌡️', '⏰', '💌', '🥇', '🥈', '🥉', '⚽',
    '🆓', '🆒', '❔', '🎦', '⏩', '▶️', '🔝', '🔜', '⬇️', '⬆️', '⛔', '🆗', '👍'
];

module.exports = {
    getRandomEmoji: function () {
        return allEmojis[Math.floor(Math.random() * allEmojis.length)];
    },
    isEmoji: function (message) {
        if (!message) return true;
        if (message.match(emojiRegex) && message.match(emojiRegex)[0]) return true;

        if (emojis.includes(message) ||
            emojis_with_consecutive_chars.includes(message)) {
            return true;
        }

        if (!message.startsWith(':') &&
            !message.startsWith('<') &&
            !message.startsWith(';')) {
            return false;
        }

        if (message.endsWith(':')) {
            return true;
        }

        for (let emoji of emojis_with_consecutive_chars) {
            if (message.startsWith(emoji)) {
                return true;
            }
        }

        return false;
    },
    isViberEmoji: function (message) {
        if (!message) return true;

        if (message.startsWith('(') && message.endsWith(')')) {
            const match = message.match('\\((.*?)\\)');
            if (!match) return false;

            return !match[0].includes(' ');
        }

        return false;
    },
    getRandomViberEmoji: function () {
        return allViberEmojis[Math.floor(Math.random() * allViberEmojis.length)];
    },
    getRandomMessengerEmoji: function () {
        return allEmojis[Math.floor(Math.random() * allEmojis.length)];
    }
};
