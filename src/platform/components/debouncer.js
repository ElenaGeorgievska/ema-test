const debounce = require('debounce');

const log = require('../../log/winston').log(__filename);

class Debouncer {
    constructor(delayMs) {
        this.scheduled = {};
        this.delayMs = delayMs;
    }

    scheduleDebounced(key, data, toExecute) {
        if (this.scheduled[key]) {
            log.debug('found executor for ', key);
            const cached = this.scheduled[key];
            const existingData = cached.data;
            existingData.push(data);
            log.debug('data is now:', existingData);
            cached.executor(toExecute, existingData);
            return;
        }

        const newExecutor = this._generateExecutor(key);
        const debouncedExecutor = debounce(newExecutor, this.delayMs);
        this.scheduled[key] = {
            executor: debouncedExecutor,
            data: [data],
        };
        debouncedExecutor(toExecute, [data]);
    }

    _generateExecutor(key) {
        log.debug(`generated new executor for ${key}`);
        return (toExecute, data) => {
            log.debug(`in executor for ${key} with data: ${JSON.stringify(data)}`);
            delete this.scheduled[key];
            toExecute(data);
        }
    }
}

module.exports = Debouncer;
