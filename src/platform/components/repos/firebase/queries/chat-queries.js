const _ = require('lodash');

class ChatQueries {
    constructor(dbReferences) {
        this.dbReferences = dbReferences;
    }

    updateUserChat({userId, chatId}, message) {
        return this.dbReferences.CHATS.child(userId).child(chatId).push(message);
    }

    async getUserChat({userId, chatId}) {
        const snap = await this.dbReferences.CHATS.child(userId).child(chatId).once('value');
        return snap.val()
    }

    async updateMessageFeed({userId, chatId}, message) {
        await this.dbReferences.MESSAGE_FEED.child(`${userId}/${chatId}`).push(message);
    }

    async getMessageFeed({userId, chatId}, limit, cursor) {
        console.log(`get last ${limit} messages, cursor ${cursor}`);
        if (!cursor) {
            const snap = await this.dbReferences.MESSAGE_FEED.child(userId).child(chatId)
                .orderByChild("timestamp").limitToLast(limit).once('value');
            const messageFeed = snap.val();
            if (messageFeed) {
                return Object.values(messageFeed);
            } else {
                return undefined;
            }
        }

        const snap = await this.dbReferences.MESSAGE_FEED.child(userId).child(chatId)
            .orderByChild("timestamp").endAt(cursor).limitToLast(limit + 1).once('value');
        const messageFeed = snap.val();
        if (!messageFeed) {
            return undefined;
        }

        let messageFeedArray = Object.values(messageFeed);

        /**
         * the cursor points to the last item in the array
         * and this item was already displayed, so we remove it from the result.
         * endAt is inclusive to the given argument (cursor)
         */
        return messageFeedArray.slice(0, messageFeedArray.length - 1);
    }

    async updateMessageFeedPreview({chatId}, messagePreview) {
        await this.dbReferences.MESSAGE_FEED_PREVIEWS.child(chatId).update(messagePreview);
    }

    async getMessageFeedPreviews(userId) {
        const snap = await this.dbReferences.MESSAGE_FEED_PREVIEWS
            .orderByChild('userId').equalTo(userId).once('value');
        return snap.val();
    }

    async getMessageFeedPreviewsForAllChats() {
        const snap = await this.dbReferences.MESSAGE_FEED_PREVIEWS
            .orderByChild('timestamp').once('value');
        return snap.val();
    }
}

module.exports = ChatQueries;
