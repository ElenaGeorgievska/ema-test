class StateQueries {
    constructor(dbReferences) {
        this.dbReferences = dbReferences;
    }

    async getCurrentStateId({userId, chatId}) {
        const stateSnap = await this.dbReferences.CURRENT_STATE
            .child(`${userId}/${chatId}`).once('value');
        if (!stateSnap.val()) return;
        const stateData = stateSnap.val();
        return stateData.path;
    }

    async setCurrentState({userId, chatId}, stateId, lang) {
        if (!lang) {
            lang = process.env.DEFAULT_LANGUAGE;
        }
        const stateSnap = await this.dbReferences.SCENARIOS_DATA
            .child(`${lang}/${stateId}`).once('value');
        if (!stateSnap.val()) return;

        const state = stateSnap.val();
        return this.dbReferences.CURRENT_STATE.child(`${userId}/${chatId}`)
            .update({data: state, path: stateId});
    }

    async getScenarios() {
        const snap = await this.dbReferences.SCENARIOS_DATA.once('value');
        return snap.val();
    }
}

module.exports = StateQueries;
