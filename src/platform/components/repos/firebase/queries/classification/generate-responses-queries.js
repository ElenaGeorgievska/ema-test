const getRandomMessage = (messages) => messages[Math.floor(Math.random() * messages.length)];

class GenerateResponsesQueries {
    constructor(dbReferences) {
        this.dbReferences = dbReferences;
    }

    async getDefaultResponse(responsesRef) {
        const snap = await responsesRef.once('value');
        const messages = snap.val();

        if (messages) {
            const message = getRandomMessage(messages);
            return [message];
        }

        return undefined;
    }

    async getResponseForProfanity(lang) {
        const responsesRef = await this.dbReferences.getProfanityResponsesRef(lang);
        const profanityResponse = await this.getDefaultResponse(responsesRef);
        if (!profanityResponse) {
            throw Error(`missing default profanity responses for the language [${lang}]`);
        }

        return profanityResponse;
    }

    async getResponseForMissclassification(lang) {
        const responsesRef = this.dbReferences.getMissclassificationResponsesRef(lang);
        const missclassificationResponse = await this.getDefaultResponse(responsesRef);
        if (!missclassificationResponse) {
            throw Error(`missing default missclassification responses for the language [${lang}]`);
        }

        return missclassificationResponse;
    }

    async getResponseForServiceUnavailable(lang) {
        const responsesRef = this.dbReferences.getServiceUnavailableResponsesRef(lang);
        const serviceUnavailableResponse = await this.getDefaultResponse(responsesRef);
        if (!serviceUnavailableResponse) {
            throw Error(`missing default service unavailable responses for the language [${lang}]`);
        }

        return serviceUnavailableResponse;
    }

    async getFaqResponseData(faqId, lang) {
        const responsesRef = await this.dbReferences.getFaqResponsesRef(lang);
        const snap = await responsesRef.child(faqId).once('value');
        const faqResponse = snap.val();
        if (!faqResponse) {
            throw Error(`missing a FAQ response for the FAQ with id: [${faqId}], language: [${lang}]`);
        }

        return faqResponse;
    }
}

module.exports = GenerateResponsesQueries;
