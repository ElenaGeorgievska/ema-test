class ClassifyingValidationQueries {
    constructor(dbReferences) {
        this.dbReferences = dbReferences;
    }

    addHit(aiServerResponse) {
        this.dbReferences.HITS.push(aiServerResponse);
    }

    addMiss(aiServerResponse) {
        this.dbReferences.MISSES.push(aiServerResponse);
    }

    addProfanity(aiServerResponse) {
        this.dbReferences.PROFANITIES.push(aiServerResponse);
    }

    updateSummaryHits() {
        this.dbReferences.VALIDATION_SUMMARY_HITS.transaction(currentValue => currentValue + 1);
        this.updateSummaryAll();
    }

    updateSummaryMisses() {
        this.dbReferences.VALIDATION_SUMMARY_MISSES.transaction(currentValue => currentValue + 1);
        this.updateSummaryAll();
    }

    updateSummaryProfanities() {
        this.dbReferences.VALIDATION_SUMMARY_PROFANITIES.transaction(currentValue => currentValue + 1);
        this.updateSummaryAll();
    }

    updateSummaryAll() {
        this.dbReferences.VALIDATION_SUMMARY_ALL.transaction(currentValue => currentValue + 1);
    }
}

module.exports = ClassifyingValidationQueries;
