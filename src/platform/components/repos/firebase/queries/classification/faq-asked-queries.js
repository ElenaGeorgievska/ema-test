class FaqAskedQueries {
    constructor(dbReferences) {
        this.dbReferences = dbReferences;
    }

    async increaseFaqAsked({userId, chatId}) {
        this.dbReferences.NUMBER_OF_FAQ_ASKED.child(userId).child(chatId).child('number')
            .transaction(currentValue => (currentValue || 0) + 1);
    }

    async getNumberOfFaqAsked({userId, chatId}) {
        const snap = await this.dbReferences.NUMBER_OF_FAQ_ASKED.child(userId).child(chatId)
            .child('number').once('value');
        return snap.val();
    }

    async resetFaqAsked({userId, chatId}) {
        await this.dbReferences.NUMBER_OF_FAQ_ASKED.child(userId).child(chatId).update({number: 0});
    }
}

module.exports = FaqAskedQueries;
