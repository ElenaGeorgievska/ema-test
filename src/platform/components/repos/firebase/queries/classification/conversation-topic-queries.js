function arraysEqual(a, b) {
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length !== b.length) return false;

    for (let i = 0; i < a.length; ++i) {
        if (a[i] !== b[i]) return false;
    }
    return true;
}

class ConversationTopicQueries {
    constructor(dbReferences) {
        this.dbReferences = dbReferences;
    }

    async saveConversationTopic({userId, chatId}, topics) {
        if (topics === undefined) {
            return this.removeConversationTopic({userId, chatId});
        }
        const snap = await this.dbReferences.CONVERSATION_TOPIC
            .child(userId).child(chatId).child('topics').once('value');
        const previousTopics = snap.val();

        if (arraysEqual(previousTopics, topics)) {
            return this.removeConversationTopic({userId, chatId});
        }

        await this.dbReferences.CONVERSATION_TOPIC.child(userId).child(chatId).update({topics});
    }

    async removeConversationTopic({userId, chatId}) {
        await this.dbReferences.CONVERSATION_TOPIC.child(userId).child(chatId).remove();
    }

    async getConversationTopic({userId, chatId}) {
        const snap = await this.dbReferences.CONVERSATION_TOPIC.child(userId).child(chatId).once('value');
        if (!snap.val()) return [''];

        const data = snap.val();
        return data.topics;
    }
}

module.exports = ConversationTopicQueries;
