class GifsQueries {
    constructor(dbReferences) {
        this.dbReferences = dbReferences;
    }

    async getGifs() {
        const snap = await this.dbReferences.GIFS_DATA.once('value');
        return snap.val();
    }
}

module.exports = GifsQueries;
