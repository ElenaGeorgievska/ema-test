const _ = require('lodash');

class UserQueries {
    constructor(dbReferences) {
        this.dbReferences = dbReferences;
    }

    async getLastEvent(userId) {
        const snap = await this.dbReferences.LAST_EVENT.child(userId).once('value');
        return snap.val();
    }

    async setLastEvent(userId, event) {
        await this.dbReferences.LAST_EVENT.child(userId).set(event);
    }

    async updateViberUserProfile(userId, profile) {
        await this.dbReferences.USER_PROFILE.child(`${userId}/viber`).update(profile);
    }

    async getViberUserProfile(userId) {
        const snap = await this.dbReferences.USER_PROFILE.child(`${userId}/viber`).once('value');
        return snap.val();
    }

    async updateLastActive(userId) {
        await this.dbReferences.USER_PROFILE.child(userId).update({lastActive: _.now()});
    }

    async getUserLanguage(userId) {
        const snap = await this.dbReferences.USER_PROFILE.child(userId).child('language').once('value');
        return snap.val();
    }

    async setUserLanguage(userId, language) {
        await this.dbReferences.USER_PROFILE.child(userId).update({language});
    }

    async updateHandoverProtocolControl(userId, controlAgent) {
        await this.dbReferences.USER_PROFILE.child(userId).update({
            control_agent: controlAgent,
            last_seen: Date.now()
        });
    }

    async saveUsersEmail(userId, email) {
        await this.dbReferences.USER_PROFILE.child(userId).update({email});
    }

    async getUsersEmail(userId) {
        const snap = await this.dbReferences.USER_PROFILE.child(userId)
            .child('email').once('value');
        return snap.val()
    }

    async saveUsersPhoneNumber(userId, phone_number) {
        await this.dbReferences.USER_PROFILE.child(userId).update({phone_number});
    }

    async getUsersPhoneNumber(userId) {
        const snap = await this.dbReferences.USER_PROFILE.child(userId)
            .child('phone_number').once('value');
        return snap.val();
    }

    async savePreviousStateData({userId, chatId}, previousState) {
        await this.dbReferences.PREVIOUS_STATES.child(`${userId}/${chatId}`).push(previousState);
    }

    async removePreviousStateData({userId, chatId}) {
        const snap = await this.dbReferences.PREVIOUS_STATES.child(`${userId}/${chatId}`)
            .orderByChild('timestamp').limitToLast(1).once('child_added');
        await this.dbReferences.PREVIOUS_STATES.child(`${userId}/${chatId}/${snap.key}`).remove();
    }

    async getPreviousStateData({userId, chatId}) {
        const snap = await this.dbReferences.PREVIOUS_STATES.child(`${userId}/${chatId}`)
            .orderByChild('timestamp').limitToLast(1).once('value');
        const data = snap.val();
        if (data) {
            const keys = Object.keys(data);
            if (keys.length > 0) {
                return data[keys[0]];
            }
        }
        return data;
    }
}

module.exports = UserQueries;
