/* Here you can find and add the database (Firebase) references */

const root = require('./config');

class DbReferences {
    constructor() {
        /** main references **/
        this.CLASSIFICATION = root.child('classifying');
        this.SCENARIOS_DATA = root.child('scenarios');
        this.GIFS_DATA = root.child('gifs');
        this.USER_DATA = root.child('user_data');
        /** end main references **/

        /** used for classify validation **/
        this.CLASSIFYING_VALIDATION = root.child('classifying_validation');
        this.HITS = this.CLASSIFYING_VALIDATION.child('hits');
        this.MISSES = this.CLASSIFYING_VALIDATION.child('misses');
        this.PROFANITIES = this.CLASSIFYING_VALIDATION.child('profanities');
        this.CLASSIFYING_VALIDATION_SUMMARY = this.CLASSIFYING_VALIDATION.child('summary');

        this.VALIDATION_SUMMARY_HITS = this.CLASSIFYING_VALIDATION_SUMMARY.child('hits');
        this.VALIDATION_SUMMARY_MISSES = this.CLASSIFYING_VALIDATION_SUMMARY.child('misses');
        this.VALIDATION_SUMMARY_PROFANITIES = this.CLASSIFYING_VALIDATION_SUMMARY.child('profanities');
        this.VALIDATION_SUMMARY_ALL = this.CLASSIFYING_VALIDATION_SUMMARY.child('all');
        /** ended classify validation **/

        /** messaging data related **/
        this.LAST_EVENT = this.USER_DATA.child('last_event');
        this.USER_PROFILE = this.USER_DATA.child('user_profile');
        this.CHATS = this.USER_DATA.child('chats');
        this.MESSAGE_FEED = this.USER_DATA.child('message_feed');
        this.MESSAGE_FEED_PREVIEWS = this.USER_DATA.child('message_feed_preview');
        this.CURRENT_STATE = this.USER_DATA.child('current_state');
        this.PREVIOUS_STATES = this.USER_DATA.child('previous_states');
        this.SAVED_SCENARIO_STATE_DATA = this.USER_DATA.child('saved_scenario_state_data');
        this.CONVERSATION_TOPIC = this.USER_DATA.child('conversation_topic');
        this.NUMBER_OF_FAQ_ASKED = this.USER_DATA.child('number_of_faq_asked');
        /** end messaging data related **/
    }

    getServiceUnavailableResponsesRef(language) {
        return this.CLASSIFICATION
            .child(`${language}/classification_responses/service_unavailable_responses`);
    }

    getMissclassificationResponsesRef(language) {
        return this.CLASSIFICATION
            .child(`${language}/classification_responses/missclassification_responses`);
    }

    getProfanityResponsesRef(language) {
        return this.CLASSIFICATION
            .child(`${language}/classification_responses/profanity_responses`)
    }

    getFaqResponsesRef(language) {
        return this.CLASSIFICATION
            .child(`${language}/classification_responses/faq_responses`);
    }

}

module.exports = DbReferences;
