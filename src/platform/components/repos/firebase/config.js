/* This is where the Firebase database is initialized. */

const admin = require('firebase-admin');

let environment = process.env.NODE_ENV;
if (!environment) {
    environment = 'development';
}

const serviceAccount = JSON.parse(process.env.SERVICE_ACCOUNT);

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: process.env.DATABASE_URL
});

const ROOT = admin.database().ref().child(environment);

module.exports = ROOT;