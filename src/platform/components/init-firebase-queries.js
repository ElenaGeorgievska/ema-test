const log = require('../../log/winston').log(__filename);

const DbReferences = require('./repos/firebase/references');

const UserQueries = require('./repos/firebase/queries/user-queries');
const ChatQueries = require('./repos/firebase/queries/chat-queries');
const GifsQueries = require('./repos/firebase/queries/gifs-queries');
const StateQueries = require('./repos/firebase/queries/state-queries');

const GenerateResponsesQueries = require('./repos/firebase/queries/classification/generate-responses-queries');
const ConversationTopicQueries = require('./repos/firebase/queries/classification/conversation-topic-queries');
const ClassifyingValidationQueries = require('./repos/firebase/queries/classification/classifying-validation-queries');
const FaqAskedQueries = require('./repos/firebase/queries/classification/faq-asked-queries');

module.exports = function () {
    log.info(`* initializing the platform firebase queries...`);

    const dbReferences = new DbReferences();

    const userQueries = new UserQueries(dbReferences);
    const chatQueries = new ChatQueries(dbReferences);
    const stateQueries = new StateQueries(dbReferences);
    const gifsQueries = new GifsQueries(dbReferences);

    const faqAskedQueries = new FaqAskedQueries(dbReferences);
    const conversationTopicQueries = new ConversationTopicQueries(dbReferences);
    const generateResponsesQueries = new GenerateResponsesQueries(dbReferences);
    const classifyingValidationQueries = new ClassifyingValidationQueries(dbReferences);

    log.info(`* platform firebase queries initialized\n`);
    return {
        userQueries,
        chatQueries,
        stateQueries,
        gifsQueries,
        faqAskedQueries,
        conversationTopicQueries,
        generateResponsesQueries,
        classifyingValidationQueries
    }
};
