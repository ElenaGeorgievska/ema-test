const validator = require('email-validator');

class EmailValidator {
    constructor() {
    }

    isValid(email) {
        return validator.validate(email);
    }

    getInvalidEmailFromText(message) {
        const re = /[a-zA-Z0-9-_.]+@[a-zA-Z0-9-_.]+/;
        return re.test(String(message).toLowerCase());
    }
}

module.exports = EmailValidator;
