const regex_00 = '[0][0]\\(?([0-9]{3})\\)?([ .-]?)([0-9]{2})([ .-]?)([0-9]{3})([ .-]?)([0-9]{3})$';
const regex_plus = '\\+\\(?([0-9]{3})\\)?([ .-]?)([0-9]{2})([ .-]?)([0-9]{3})([ .-]?)([0-9]{3})$';
const regex_3 = '[3]\\(?([0-9]{2})\\)?([ .-]?)([0-9]{2})([ .-]?)([0-9]{3})([ .-]?)([0-9]{3})$';
const regex_0 = '[0][7][0-9]([ .-]?)([0-9]{3})([ .-]?)([0-9]{3})$';
const regex_7 = '[7][0-9]([ .-]?)([0-9]{3})([ .-]?)([0-9]{3})$';

class PhoneNumberValidator {
    constructor() {
    }

    hasValidPhoneNumber(message) {
        message = message.replace(/[^\d-]/g, '');

        let result = message.match(regex_00);
        if (result) {
            return result[0];
        }

        result = message.match(regex_plus);
        if (result) {
            return result[0];
        }

        result = message.match(regex_3);
        if (result) {
            return result[0];
        }

        result = message.match(regex_0);
        if (result) {
            return result[0];
        }

        result = message.match(regex_7);
        if (result) {
            return result[0];
        }
    }

    matchInvalidPhoneNumberInText(message) {
        const regex = new RegExp('\\d+');
        const match = message.match(regex);
        if (match && match.length > 0) {
            return match[0];
        }
    }
}

module.exports = PhoneNumberValidator;
