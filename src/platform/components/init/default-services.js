const log = require('../../../log/winston').log(__filename);

const UserService = require('../services/user-service');
const ChatService = require('../services/chat-service');
const StateService = require('../services/state-service');
const GifsService = require('../services/gifs-service');

module.exports = function (components, {userQueries, chatQueries, stateQueries, gifsQueries}) {
    log.info(`default services`);

    components.userService = new UserService(userQueries);
    components.chatService = new ChatService(chatQueries);
    components.gifsService = new GifsService(gifsQueries);
    components.stateService = new StateService(stateQueries, components);
};
