const log = require('../../../log/winston').log(__filename);

const PhoneNumberValidator = require('../validators/phone-number-validator');
const EmailValidator = require('../validators/email-validator');

module.exports = function (components) {
    log.info(`init validators`);

    components.phoneNumberValidator = new PhoneNumberValidator();
    components.emailValidator = new EmailValidator();
};
