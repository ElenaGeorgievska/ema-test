const {PubSub} = require('apollo-server-express');

const log = require('../../../log/winston').log(__filename);

const initClassificationServices = require('./classification-services');
const initDefaultServices = require('./default-services');
const initGetStateServices = require('./get-state-services');
const initValidators = require('./validators');
const initTransferToAgentServices = require('./transfer-to-agent-services');

const StateTraversalService = require('../services/traversal-service');

const emojis = require('../emojis');

module.exports = async function (queries) {
    log.info(`* initializing the components...`);

    let components = {};
    components.emojis = emojis;
    components.pubSub = new PubSub();

    initValidators(components);
    initClassificationServices(components, queries);
    initTransferToAgentServices(components);
    initGetStateServices(components);
    initDefaultServices(components, queries);
    components.stateTraversalService = new StateTraversalService(components);

    return components;
};
