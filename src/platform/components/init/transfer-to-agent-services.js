const log = require('../../../log/winston').log(__filename);

const HandoverProtocolService = require('../services/transfer_to_agent/handover-protocol-service');

module.exports = function (components) {
    log.info(`handover protocol agent services`);
    components.handoverProtocolService = new HandoverProtocolService(components.userService);
};
