const log = require('../../../log/winston').log(__filename);

const ClassifyApiService = require('../services/classification/classify-api-service');
const GetResponseHandlerService = require('../services/classification/get-response-handler-service');
const ConversationTopicService = require('../services/classification/conversation-topic-service');
const ClassifyingValidationService = require('../services/classification/classifying-validation-service');
const FaqAskedService = require('../services/classification/faq-asked-service');
const GenerateResponsesService = require('../services/classification/generate-responses-service');

module.exports = function (components, {
    faqAskedQueries,
    conversationTopicQueries,
    generateResponsesQueries,
    classifyingValidationQueries
}) {
    log.info(`classification services`);

    components.faqAskedService = new FaqAskedService(faqAskedQueries);
    components.classifyingValidationService = new ClassifyingValidationService(classifyingValidationQueries);
    components.conversationTopicService = new ConversationTopicService(conversationTopicQueries);
    components.generateResponsesService = new GenerateResponsesService(generateResponsesQueries);
    components.getResponseHandlerService = new GetResponseHandlerService(components);

    components.classifyApiService = new ClassifyApiService(components);
};
