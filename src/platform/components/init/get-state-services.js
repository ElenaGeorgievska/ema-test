const log = require('../../../log/winston').log(__filename);

const GetStateService = require('../services/get_state/get-state-service');
const DefaultStateProvider = require('../states/default/state-provider');

module.exports = function (components) {
    log.info(`default get state services`);

    const defaultStateProvider = new DefaultStateProvider(components);
    components.getStateService = new GetStateService(
        components, [defaultStateProvider]
    );
};
