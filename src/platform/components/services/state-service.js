const log = require('../../../log/winston').log(__filename);

class StateService {
    constructor(stateQueries, {getStateService}) {
        this.stateQueries = stateQueries;
        this._userToState = {};
        this.getStateService = getStateService;
        this._states = {};
        this._getScenarios()
            .then(() => log.info(`scenarios loaded successfully...`))
            .catch((err) => log.error(`${err}`));
    }

    async getCurrentStateId({userId, chatId}) {
        if (!this._userToState[userId]) {
            this._userToState[userId] = {};
        }

        if (!this._userToState[userId][chatId]) {
            log.debug(`asking db, current state, user [${userId}], chat [${chatId}]`);
            const currentStateID = await this.stateQueries.getCurrentStateId({userId, chatId});
            if (!currentStateID) return undefined;

            this._userToState[userId][chatId] = currentStateID;
        }

        return this._userToState[userId][chatId];
    }

    async setCurrentStateId({userId, chatId}, stateId, lang) {
        log.debug(`writing to db, user: [${userId}], chat: [${chatId}], state [${stateId}]`);
        if (!this._userToState[userId]) {
            this._userToState[userId] = {};
        }

        this._userToState[userId][chatId] = stateId;
        await this.stateQueries.setCurrentState({userId, chatId}, stateId, lang);
    }

    getStateData(stateId, language) {
        let statesForLanguage = this._states[language];
        if (!statesForLanguage) {
            statesForLanguage = this._states[process.env.DEFAULT_LANGUAGE];
        }

        return statesForLanguage[stateId]
    }

    isSameScenario(currentStateId, nextStateId, language) {
        const currentState = this.getStateData(currentStateId, language);
        const nextState = this.getStateData(nextStateId, language);
        if (!currentState || !nextState) return undefined;

        return currentState.scenario.name === nextState.scenario.name;
    }

    async getState(stateId, language) {
        if (!language) {
            language = process.env.DEFAULT_LANGUAGE;
        }

        log.debug(`get state with id: ${stateId}, lang: ${language}`);

        if (!this._states[language]) {
            await this._getScenarios();
        }
        const statesForLanguage = this._states[language];

        if (!statesForLanguage[stateId]) {
            stateId = process.env.ONBOARDING_STATE;
        }

        const state = statesForLanguage[stateId];

        const stateData = {
            id: stateId,
            messages: state.messages,
            choices: state.choices,
            type: state.type,
            returnMessages: state.returnMessages,
            gifType: state.gifType,
            delayInMs: state.delayInMs,
            classifyType: state.classifyType,
            topic: state.topic,
            scenario: state.scenario
        };

        if (stateData.type) {
            stateData.type = stateData.type.toLowerCase();
        }

        return await this.getStateService.getState(stateData);
    }

    getNoScenariosDataState() {
        return this.getStateService.getNoScenariosDataState();
    }

    missingScenariosData() {
        return !this._states || Object.keys(this._states).length === 0;
    }

    async _getScenarios() {
        log.info(`downloading scenarios...`);
        this._states = await this.stateQueries.getScenarios();
        if (!this._states) {
            throw Error(`missing scenarios data!`)
        }
    }
}

module.exports = StateService;
