const DefaultState = require('../../states/default/impl/default-state');
const NoScenariosDataState = require('../../states/no-scenarios-data-state');
const State = require('../../states/state');

class GetStateService {
    constructor(components, defaultProviders) {
        this._components = components;
        this._providers = [...defaultProviders];
        this._DefaultStateClass = DefaultState;
    }

    getState(stateData) {
        if (!stateData.type) {
            return new this._DefaultStateClass(stateData, this._components);
        }

        for (let provider of this._providers) {
            const stateModel = provider.getState(stateData);
            if (stateModel) {
                return stateModel;
            }
        }
        return new DefaultState(stateData, this._components);
    }

    addProviders(...providers) {
        for (let provider of providers) {
            if (!provider.getState) {
                throw 'getState method missing in provider: ' + provider;
            }
            this._providers.unshift(provider);
        }
    }

    setDefaultState(DefaultStateClassOverride) {
        if (!(DefaultStateClassOverride.prototype instanceof State)) {
            throw 'Class ' + DefaultStateClassOverride + ' does not extend /platform/components/state/State';
        }
        this._DefaultStateClass = DefaultStateClassOverride;
    }

    getNoScenariosDataState() {
        return new NoScenariosDataState(this._components);
    }
}

module.exports = GetStateService;
