class ConversationTopicService {
    constructor(classificationQueries) {
        this.classificationQueries = classificationQueries;
    }

    async saveConversationTopic({userId, chatId}, conversationTopic) {
        await this.classificationQueries.saveConversationTopic({userId, chatId}, conversationTopic);
    }

    async removeConversationTopic({userId, chatId}) {
        await this.classificationQueries.removeConversationTopic({userId, chatId});
    }

    async getConversationTopic({userId, chatId}) {
        return await this.classificationQueries.getConversationTopic({userId, chatId});
    }
}

module.exports = ConversationTopicService;
