class GenerateResponsesService {
    constructor(generateResponsesQueries) {
        this.generateResponsesQueries = generateResponsesQueries;
    }

    async getResponseForProfanity(lang) {
        return await this.generateResponsesQueries.getResponseForProfanity(lang);
    }

    async getResponseForMissclassification(lang) {
        return await this.generateResponsesQueries.getResponseForMissclassification(lang);
    }

    async getResponseForServiceUnavailable(lang) {
        return await this.generateResponsesQueries.getResponseForServiceUnavailable(lang);
    }

    async getFaqResponseData(faqId, lang) {
        return await this.generateResponsesQueries.getFaqResponseData(faqId, lang);
    }
}

module.exports = GenerateResponsesService;
