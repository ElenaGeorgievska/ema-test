const log = require('../../../../log/winston').log(__filename);

const FaqResponseHandler = require('./response_handlers/faq-handler');
const ScenarioResponseHandler = require('./response_handlers/scenario-handler');
const MissclassificationResponseHandler = require('./response_handlers/missclassification-handler');
const ProfanityResponseHandler = require('./response_handlers/profanity-handler');
const ServiceUnavailableHandler = require('./response_handlers/service-unavailable-handler');

const RESPONSE_TYPE = {
    FAQ: 'faq',
    SCENARIO: 'scenario',
    MISSCLASSIFICATION: 'missclassification',
    PROFANITY: 'profanity'
};

class GetResponseHandlerService {
    constructor(components) {
        this.components = components;
    }

    async getHandler({userId, chatId}, response, currentState) {
        switch (response.type) {
            case RESPONSE_TYPE.FAQ:
                return new FaqResponseHandler(
                    {userId, chatId}, response, currentState, this.components
                );
            case RESPONSE_TYPE.SCENARIO:
                return new ScenarioResponseHandler(
                    {userId, chatId}, response, currentState, this.components
                );
            case RESPONSE_TYPE.MISSCLASSIFICATION:
                return new MissclassificationResponseHandler(
                    {userId, chatId}, response, currentState, this.components
                );
            case RESPONSE_TYPE.PROFANITY:
                return new ProfanityResponseHandler(
                    {userId, chatId}, response, currentState, this.components
                );
        }
    }

    async getServiceUnavailableHandler() {
        return new ServiceUnavailableHandler(this.components);
    }
}

module.exports = GetResponseHandlerService;
