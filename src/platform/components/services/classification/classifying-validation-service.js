class ClassifyingValidationService {
    constructor(classifyingValidationQueries) {
        this.classifyingValidationQueries = classifyingValidationQueries;
    }

    addHit(aiServerResponse) {
        if (!aiServerResponse) return;

        const results = aiServerResponse.results;
        if (!results || results.length === 0) return;

        this.classifyingValidationQueries.addHit(aiServerResponse);
        this.classifyingValidationQueries.updateSummaryHits();
    }

    addMiss(aiServerResponse) {
        this.classifyingValidationQueries.addMiss(aiServerResponse);
        this.classifyingValidationQueries.updateSummaryMisses();
    }

    addProfanity(aiServerResponse) {
        this.classifyingValidationQueries.addProfanity(aiServerResponse);
        this.classifyingValidationQueries.updateSummaryProfanities();
    }
}

module.exports = ClassifyingValidationService;
