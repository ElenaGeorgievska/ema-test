const _ = require('lodash');

const log = require('../../../../../log/winston').log(__filename);

const {ClassificationResult} = require('../../../../models/classification-result');

const ResponseHandlerService = require('./handler');

class ScenarioHandler extends ResponseHandlerService {
    constructor(
        {userId, chatId},
        response,
        currentState,
        {classifyingValidationService, faqAskedService}
    ) {
        super(
            {userId, chatId},
            response,
            currentState,
            {classifyingValidationService, faqAskedService}
        );
        this.currentState = currentState;
        const {results} = response;
        const bestResult = results[0];

        this.selectedChoiceId = bestResult.class_id;
    }

    async _getResultToReturn() {
        const choices = this.currentState.choices;
        const choice = await _.find(choices, choice => choice.id === this.selectedChoiceId);
        if (!choice) {
            throw Error(`received a wrong choice id from the ai server.`);
        }

        return ClassificationResult.Action({
            link: choice.link,
            value: choice.value
        });
    }

    async generateResponse() {
        log.debug(`scenario response received`);

        const userId = this.userId;
        const chatId = this.chatId;

        await this.faqAskedService.resetFaqAsked({userId, chatId});
        await this.classifyingValidationService.addHit(this.response);
        return await this._getResultToReturn();
    }
}

module.exports = ScenarioHandler;
