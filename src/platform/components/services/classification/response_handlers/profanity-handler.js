const log = require('../../../../../log/winston').log(__filename);

const ResponseHandlerService = require('./handler');

class ProfanityHandler extends ResponseHandlerService {
    constructor(
        {userId, chatId},
        response,
        currentState,
        {
            conversationTopicService,
            generateResponsesService,
            classifyingValidationService,
            faqAskedService
        }
    ) {
        super(
            {userId, chatId},
            response,
            currentState,
            {classifyingValidationService, faqAskedService}
        );
        this.currentState = currentState;

        this.generateResponsesService = generateResponsesService;
        this.conversationTopicService = conversationTopicService;
    }

    async _getResultToReturn() {
        try {
            const messages = await this.generateResponsesService.getResponseForProfanity(this.lang);
            return await super.returnDefaultResponsesMessage(messages);
        } catch (e) {
            log.error(e);
        }
    }

    async generateResponse() {
        const userId = this.userId;
        const chatId = this.chatId;

        log.debug(`profanity response received, user: [${userId}], chat: [${chatId}]`);

        this.faqAskedService.increaseFaqAsked({userId, chatId});
        await this.conversationTopicService.removeConversationTopic({userId, chatId});
        await this.classifyingValidationService.addProfanity(this.response);

        return await this._getResultToReturn();
    }

}

module.exports = ProfanityHandler;
