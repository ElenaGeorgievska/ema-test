const log = require('../../../../../log/winston').log(__filename);

const {ClassificationResult} = require('../../../../models/classification-result');
const {Message} = require('../../../../models/message');

class ServiceUnavailableHandler {
    constructor({generateResponsesService}) {
        this.generateResponsesService = generateResponsesService;
    }

    async generateResponse(lang) {
        try {
            const messages = await this.generateResponsesService.getResponseForServiceUnavailable(lang);
            const messageObj = Message.TextMessages(messages);
            return ClassificationResult.Wait(messageObj);
        } catch (e) {
            log.error(e);
        }
    }

}

module.exports = ServiceUnavailableHandler;
