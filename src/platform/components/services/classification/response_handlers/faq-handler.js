const _ = require('lodash');

const log = require('../../../../../log/winston').log(__filename);

const {ClassificationResult} = require('../../../../models/classification-result');
const {Message} = require('../../../../models/message');

const ResponseHandlerService = require('./handler');

class FaqResponseHandlerService extends ResponseHandlerService {
    constructor(
        {userId, chatId},
        response,
        currentState,
        {
            conversationTopicService,
            faqAskedService,
            classifyingValidationService,
            generateResponsesService,
            userService,
        }
    ) {
        super(
            {userId, chatId},
            response,
            currentState,
            {classifyingValidationService, faqAskedService}
        );

        this.currentState = currentState;
        this.bestResult = this.response.results[0];

        this.generateResponsesService = generateResponsesService;
        this.conversationTopicService = conversationTopicService;

        this.userService = userService;
    }

    async generateResponse() {
        const userId = this.userId;
        const chatId = this.chatId;
        log.debug(`FAQ response received, user: [${userId}], chat: [${chatId}]`);

        await this.faqAskedService.increaseFaqAsked({userId, chatId});
        await this.classifyingValidationService.addHit(this.response);

        return await this._getResultToReturn();
    }


    async _getResultToReturn() {
        try {
            const classId = this.bestResult.class_id;
            const faqResponse = await this.generateResponsesService.getFaqResponseData(classId, this.lang);

            let handlingType;

            if (this._shouldRespondAndSwitchState(faqResponse)) {
                handlingType = 'sendMessageAndStartScenario';
            } else if (faqResponse.link === 'link') {
                handlingType = 'sendMessage';
            } else {
                handlingType = 'startScenario';
            }

            faqResponse.handlingType = handlingType;

            return await this._returnResult(faqResponse);
        } catch (e) {
            log.error(e);
        }
    }

    async _returnResult({link, event_type, conversation_topics, replies, handlingType}) {
        const userId = this.userId;
        const chatId = this.chatId;

        await this.conversationTopicService.saveConversationTopic({userId, chatId}, conversation_topics);

        if (handlingType === 'startScenario') {
            console.log(`current state: `, JSON.stringify(this.currentState));
            await this.userService.savePreviousStateData({userId, chatId}, this.currentState);
            return ClassificationResult.SwitchState(link);
        }

        let choices = undefined;
        if (this.currentState) {
            choices = this.currentState.choices;
        }
        let messages = this._getRandomResponseMessage(replies);

        if (handlingType === 'sendMessageAndStartScenario') {
            await this.userService.savePreviousStateData({userId, chatId}, this.currentState);
            const messageObj = Message.TextMessages(messages, choices);
            return ClassificationResult.SwitchState(link, 0, messageObj);
        }

        if (handlingType === 'sendMessage') {
            return await super.returnDefaultResponsesMessage(messages);
            // const returnToScenarioMessages = await this.faqAskedService
            //     .returnToScenario({userId, chatId}, this.currentState);
            // if (returnToScenarioMessages) {
            //     messages = messages.concat(returnToScenarioMessages);
            // }
            //
            // const messageObj = Message.TextMessages(messages, choices);
            // return ClassificationResult.Wait(messageObj);
        }
    }

    _shouldRespondAndSwitchState({link, replies}) {
        if (!replies) {
            return false;
        }

        return link && link !== 'link' &&
            replies[0] && replies[0][0].length > 1;
    }

    _getRandomResponseMessage(replies) {
        return replies[_.random(replies.length - 1)];
    }
}

module.exports = FaqResponseHandlerService;
