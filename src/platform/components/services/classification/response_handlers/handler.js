const {ClassificationResult} = require('../../../../models/classification-result');
const {Message} = require('../../../../models/message');

const getRandomMessage = (array) => array[Math.floor(Math.random() * array.length)];

class Handler {
    constructor(
        {userId, chatId},
        response,
        currentState,
        {classifyingValidationService, faqAskedService}
    ) {
        this.userId = userId;
        this.chatId = chatId;
        this.response = response;
        this.currentState = currentState;
        this.lang = response.lang;
        this.type = response.type;

        this.classifyingValidationService = classifyingValidationService;
        this.faqAskedService = faqAskedService;

        if (this._getResultToReturn === undefined) {
            throw TypeError(`must implement method 'generateBotResponse'`);
        }

        if (this.generateResponse === undefined) {
            throw TypeError(`must implement method 'sendBotResponse'`);
        }
    }

    _getResultToReturn() {
    }

    generateResponse() {
    }

    async _addStatesReturnMessages(messages) {
        if (!this.currentState) return messages;

        const faqAsked = await this.faqAskedService
            .getNumberOfFaqAsked({userId: this.userId, chatId: this.chatId});

        if (faqAsked <= 3) return messages;

        await this.faqAskedService.resetFaqAsked({userId: this.userId, chatId: this.chatId});

        const returnMessagesState = this.currentState.returnMessages;
        if (returnMessagesState) {
            const randomReturnMessage = getRandomMessage(returnMessagesState);
            messages = messages.concat(randomReturnMessage);
        }

        return messages;
    }

    _addStatesChoices() {
        if (this.currentState) {
            return this.currentState.choices;
        }
    }

    async returnDefaultResponsesMessage(messages) {
        messages = await this._addStatesReturnMessages(messages);
        const choices = this._addStatesChoices();

        const messageObj = Message.TextMessages(messages, choices);
        return ClassificationResult.Wait(messageObj);
    }
}

module.exports = Handler;
