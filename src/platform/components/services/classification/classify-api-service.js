const axios = require('axios');

const log = require('../../../../log/winston').log(__filename);

const HEADERS = {
    'Content-Type': 'application/json',
    'Accept-Charset': 'utf-8'
};

const REQUEST_TYPE = {
    SCENARIO: 'scenario',
    FAQ: 'faq'
};

const url = `${process.env.CLASSIFY_SERVER_URL}/classify`;

class ClassifyApiService {
    constructor({getResponseHandlerService, conversationTopicService}) {
        this.getResponseHandlerService = getResponseHandlerService;
        this.conversationTopicService = conversationTopicService;
    }

    async runClassifying({userId, chatId, message, language}, currentStateData) {
        try {
            const data = await this._generateRequestData(
                {userId, chatId, message, language}, currentStateData
            );
            const aiResponse = await axios.post(
                `${url}/${language}`,
                data,
                {
                    headers: HEADERS
                });

            if (aiResponse.status === 200) {
                log.debug(`response received from ai server: ${JSON.stringify(aiResponse.data)}`);

                const handler = await this.getResponseHandlerService
                    .getHandler({userId, chatId}, aiResponse.data, currentStateData);
                return await handler.generateResponse();
            } else {
                throw Error(`an error received from the ai server, status: [${aiResponse.status}]`);
            }
        } catch (err) {
            log.error(`error in runClassifying: [${err}]`);

            if (err.statusCode === 502) {
                log.error('bad gateway, please restart the ai server!!');
            }

            log.debug(`sending service unavailable response for user: [${userId}] in chat: [${chatId}]`);
            const handler = await this.getResponseHandlerService
                .getServiceUnavailableHandler();
            return await handler.generateResponse(language);
        }
    }

    async _generateRequestData({userId, chatId, message, language}, currentStateData) {
        const conversationTopic = await this.conversationTopicService.getConversationTopic({userId, chatId});

        let data = {
            language,
            question: message
        };
        if (this._hasNoStateOrChoices(currentStateData)) {
            data.type = REQUEST_TYPE.FAQ;
            data.conversation_topics = conversationTopic;
        } else {
            data.type = REQUEST_TYPE.SCENARIO
            if (currentStateData.classifyType) {
                data.classify_type = currentStateData.classifyType
            }
            data.state_data = {
                id: currentStateData.id,
                choices: currentStateData.choices
            };
        }

        return data
    }

    _hasNoStateOrChoices(currentStateData) {
        return !currentStateData || !currentStateData.choices || currentStateData.choices.length === 0;
    }
}

module.exports = ClassifyApiService;
