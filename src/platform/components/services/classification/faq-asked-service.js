class FaqAskedService {
    constructor(faqAskedQueries) {
        this.faqAskedQueries = faqAskedQueries;
    }

    increaseFaqAsked({userId, chatId}) {
        this.faqAskedQueries.increaseFaqAsked({userId, chatId});
    }

    async getNumberOfFaqAsked({userId, chatId}) {
        return await this.faqAskedQueries.getNumberOfFaqAsked({userId, chatId});
    }

    async resetFaqAsked({userId, chatId}) {
        await this.faqAskedQueries.resetFaqAsked({userId, chatId});
    }
}

module.exports = FaqAskedService;
