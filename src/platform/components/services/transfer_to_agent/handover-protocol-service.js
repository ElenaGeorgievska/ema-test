const log = require('../../../../log/winston').log(__filename);

const {MESSENGER_GRAPH_API_URL, MESSENGER_SEND_API_URL} = require('../../../api/messenger/constants');

class HandoverProtocolService {
    constructor(userService) {
        this.userService = userService;
    }

    callSendAPI(userPsid, response) {
        response = {
            text: response
        };

        let request_body = {
            recipient: {
                id: userPsid
            },
            message: response,
            metadata: 'DO NOT Pass control to the Page Inbox'
        };

        request({
            uri: MESSENGER_SEND_API_URL,
            qs: {
                'access_token': process.env.MESSENGER_PAGE_ACCESS_TOKEN
            },
            method: 'POST',
            json: request_body
        }, err => {
            if (!err)
                return log.debug(`handover message successfully sent for user [${userPsid}]`);

            log.error(`unable to send handover message for user [${userPsid}]: ${err}`);
        });
    };

    sendChangeControlEvent(path, payload) {
        log.debug(`sending change control event... ${JSON.stringify(payload)}`);
        const access_token = process.env.MESSENGER_PAGE_ACCESS_TOKEN;

        if (!path) {
            return log.error('no endpoint (path) specified on messenger send!');
        }

        if (!access_token || !MESSENGER_GRAPH_API_URL) {
            return log.error('no page access token or graph api url configured!');
        }

        request({
            uri: MESSENGER_GRAPH_API_URL + path,
            qs: {
                'access_token': access_token
            },
            method: 'POST',
            json: payload,
        }, (err, res) => {
            if (!err && res.statusCode === 200) {
                return log.debug('change control event sent successfully!');
            }

            log.error(`change control event, error: ${JSON.stringify(err)}`);
        });
    };

    /**
     * Passing control to the secondary receiver(page inbox, agent...)
     * update the user with flag to know that the control is in the secondary receiver(agent)
     */
    async passControl(userId, response) {
        this.callSendAPI(userId, response);
        await this.userService.updateHandoverProtocolControl(userId, true);

        this.sendChangeControlEvent('/pass_thread_control', {
            recipient: {
                id: userId
            },
            target_app_id: process.env.MESSENGER_PAGE_INBOX_APP_ID
        });
    };

    /**
     * Taking control from the secondary receiver (now the control is in the script(bot))
     * update the user with flag to know that the control is in the primary receiver(script, bot)
     */
    async takeControl(userId) {
        await this.userService.updateHandoverProtocolControl(userId, false);

        this.sendChangeControlEvent('/take_thread_control', {
            recipient: {
                id: userId
            }
        });
    };
}


module.exports = HandoverProtocolService;
