const log = require('../../../log/winston').log(__filename);

const getRandomGif = (gifs) => gifs[Math.floor(Math.random() * gifs.length)];

class GifsService {
    constructor(gifsQueries) {
        this.gifsQueries = gifsQueries;
        this._gifs = {};
        this._getGifs()
            .then(() => log.info(`gifs loaded successfully...`))
            .catch((err) => log.error(`${err}`));
    }

    async getGif(type) {
        if (!this._gifs) {
            await this._getGifs();
        }
        const gifsByType = this._gifs[type];
        if (!gifsByType) {
            return undefined
        }

        return getRandomGif(gifsByType);
    }

    async _getGifs() {
        log.info(`downloading gifs...`);
        this._gifs = await this.gifsQueries.getGifs();
        if (!this._gifs) {
            throw Error(`missing gifs data!`)
        }
    }
}

module.exports = GifsService;
