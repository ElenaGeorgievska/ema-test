const _ = require('lodash');
const uuidv4 = require('uuid').v4;

const DEFAULT_CHAT_ID = 'default_chat_id';

const log = require('../../../log/winston').log(__filename);

const {MESSAGE_TYPE_ENUM} = require('../../api/enums');
const {MessageRepresentation, CarouselElement, CarouselPayloadButton} = require('../../api/web/message-representation');
const {MessagePreview} = require('../../models/message-preview');
const {MSG_TYPE} = require('../../models/message');

const generateDefaultChatId = function (userId) {
    return userId + '_' + DEFAULT_CHAT_ID
}

class ChatService {
    constructor(chatQueries) {
        this.chatQueries = chatQueries;
    }

    createNewChat() {
        // return random chatId
        return uuidv4() + '-' + Date.now();
    }

    updateUserChat({userId, chatId}, message) {
        chatId = _.defaultTo(chatId, generateDefaultChatId(userId));
        this.chatQueries.updateUserChat({userId, chatId}, message);
    }

    async getUserChat({userId, chatId}) {
        chatId = _.defaultTo(chatId, generateDefaultChatId(userId));
        return await this.chatQueries.getUserChat({userId, chatId});
    }

    async updateMessageFeed({userId, chatId}, messageObj) {
        chatId = _.defaultTo(chatId, generateDefaultChatId(userId));
        const keys = Object.keys(messageObj);
        keys.forEach(key => {
            if (messageObj[key] === undefined) {
                delete messageObj[key];
            }
        });

        let messagePreview;
        switch (messageObj.type) {
            case MSG_TYPE.URL:
                messagePreview = MessagePreview.UrlMessage(userId, messageObj);
                break;
            case MSG_TYPE.USER:
                messagePreview = MessagePreview.UserMessage(userId, messageObj);
                break;
            case MSG_TYPE.TEXT:
                messagePreview = MessagePreview.TextMessages(userId, messageObj);
                break;
            case MSG_TYPE.CAROUSEL:
                messagePreview = MessagePreview.CarouselElements(userId, messageObj);
                break;
        }

        await this.chatQueries.updateMessageFeed({userId, chatId}, messageObj);
        if (messagePreview) {
            await this.chatQueries.updateMessageFeedPreview({chatId}, messagePreview);
        }
    }

    async getMessageFeed({userId, chatId}, limit, cursor) {
        if (!limit) {
            limit = parseInt(process.env.MESSAGE_FEED_LIMIT);
        }
        chatId = _.defaultTo(chatId, generateDefaultChatId(userId));

        const messageFeed = await this.chatQueries.getMessageFeed({userId, chatId}, limit, cursor);
        if (!messageFeed) {
            return {feed: []};
        }

        return this._parseMessages({userId, chatId}, messageFeed);
    }

    async getMessageFeedPreviews(userId) {
        const messageFeedPreviews = await this.chatQueries.getMessageFeedPreviews(userId);
        if (!messageFeedPreviews) {
            return []
        }

        const result = [];

        for (const chatId of Object.keys(messageFeedPreviews)) {
            const preview = messageFeedPreviews[chatId];
            preview.chatId = chatId;
            result.push(preview);
        }

        return _.orderBy(result, ['timestamp'], 'desc');
    }

    _parseMessages({userId, chatId}, messageFeed) {
        let cursor;
        const parsedMessages = [];

        for (const item of messageFeed) {
            if (!cursor) {
                cursor = item.timestamp;
            }

            if (item.type === MESSAGE_TYPE_ENUM.USER) {
                const msg = this._parseUserMsg(item);
                parsedMessages.push(msg);
            } else if (item.type === MESSAGE_TYPE_ENUM.TEXT) {
                const messages = this._parseTextMsg(item, chatId);
                parsedMessages.concat(messages);
            } else if (item.type === MESSAGE_TYPE_ENUM.URL) {
                const {url, urlType, og} = item.urlObj;
                const msg = MessageRepresentation.Url({chatId, buttons: item.choices}, {url, urlType, og});
                parsedMessages.push(msg);
            } else if (item.type === MESSAGE_TYPE_ENUM.CAROUSEL) {
                const msg = this._parseCarouselMsg(item, chatId);
                parsedMessages.push(msg);
            } else {
                log.warn(`missing implementation for this chat item: ${JSON.stringify(item)}`);
            }

        }

        return {feed: parsedMessages, cursor};
    }

    _parseUserMsg(item) {
        const {
            chatId, hidden, link, message,
            source, timestamp, type, userId, value
        } = item.messages;
        return MessageRepresentation.User({userId, chatId, timestamp}, {
            type,
            hidden,
            link,
            source,
            value,
            message
        });
    }

    _parseTextMsg(item, chatId) {
        const parsed = [];
        const itemMessages = item.messages;
        for (let i = 0; i < itemMessages.length; i++) {
            const textMessage = itemMessages[i];
            let choices;
            if (i === item.messages.length - 1) {
                choices = item.choices;
            }
            const msg = MessageRepresentation.Text({
                chatId,
                buttons: choices
            }, textMessage)
            parsed.push(msg);
        }
        return parsed;
    }

    _parseCarouselMsg(item, chatId) {
        const carouselElements = [];
        for (const element of item.carouselElements) {
            const {title, image_url, subtitle} = element;
            const carouselButtons = element.buttons;
            let carouselButton;
            if (carouselButtons) {
                const button = carouselButtons[0];
                carouselButton = new CarouselPayloadButton({
                    title: button.title,
                    type: button.type,
                    payload: button.payload
                });
            }

            const carouselElement = new CarouselElement({
                title,
                description: subtitle,
                imageUrl: image_url,
                button: carouselButton
            });
            carouselElements.push(carouselElement);
        }
        return MessageRepresentation.Carousel({
            chatId,
            buttons: item.choices
        }, carouselElements);
    }
}

module.exports = ChatService;
