const NodeCache = require('node-cache');
const _ = require('lodash');

const cachingTimeInSec = 5 * 60;
const languageCache = new NodeCache({stdTTL: cachingTimeInSec});

class UserService {
    constructor(userQueries) {
        this.userQueries = userQueries;
    }

    async getLastEvent(userId) {
        return await this.userQueries.getLastEvent(userId);
    }

    async setLastEvent(userId, event) {
        await this.userQueries.setLastEvent(userId, event);
    }

    async updateViberUserProfile(userId, profile) {
        await this.userQueries.updateViberUserProfile(userId, profile);
    }

    async getViberUserProfile(userId) {
        return await this.userQueries.getViberUserProfile(userId);
    }

    async updateUserLastActive(userId) {
        await this.userQueries.updateLastActive(userId);
    }

    async getUserLanguage(userId) {
        let language = languageCache.get(userId);
        if (language) {
            return language;
        }

        language = await this.userQueries.getUserLanguage(userId);
        if (!language) {
            language = process.env.DEFAULT_LANGUAGE;
        }

        languageCache.set(userId, language);
        return language;
    }

    async setUserLanguage(userId, language) {
        await this.userQueries.setUserLanguage(userId, language);
        languageCache.set(userId, language);
    }

    updateHandoverProtocolControl(userId, controlAgent) {
        return this.userQueries.updateHandoverProtocolControl(userId, controlAgent);
    }

    saveUsersEmail(userId, email) {
        this.userQueries.saveUsersEmail(userId, email);
    }

    async getUsersEmail(userId) {
        return await this.userQueries.getUsersEmail(userId);
    }

    saveUsersPhoneNumber(userId, phoneNumber) {
        this.userQueries.saveUsersPhoneNumber(userId, phoneNumber);
    }

    async getUsersPhoneNumber(userId) {
        return await this.userQueries.getUsersPhoneNumber(userId);
    }

    async savePreviousStateData({userId, chatId}, currentState) {
        if (!currentState) return undefined;
        if (!currentState.scenario.important) return undefined;

        const previousStateData = {
            stateId: currentState.id,
            timestamp: Date.now()
        };

        if (currentState.topic) {
            previousStateData.topic = currentState.topic;
        } else if (currentState.scenario.topic) {
            previousStateData.topic = currentState.scenario.topic;
        }

        if (currentState.onSuccess) {
            previousStateData.onSuccess = currentState.onSuccess;
        }

        if (currentState.onFail) {
            previousStateData.onFail = currentState.onFail;
        }

        if (currentState.stateId) {
            previousStateData.stateId = currentState.stateId;
        }

        await this.userQueries.savePreviousStateData({userId, chatId}, previousStateData);
    }

    async getPreviousStateData({userId, chatId}) {
        return await this.userQueries.getPreviousStateData({userId, chatId});
    }

    async removePreviousStateData({userId, chatId}) {
        return await this.userQueries.removePreviousStateData({userId, chatId});
    }
}

module.exports = UserService;
