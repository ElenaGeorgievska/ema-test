const log = require('../../../log/winston').log(__filename);

const {EVENT_RESULT_TYPE, EventResult} = require('../../models/event-result');

class TraversalService {
    constructor({stateService, userService}) {
        this.stateService = stateService;
        this.userService = userService;

        this.switches = {};
    }

    clearStateSwitches({userId, chatId}) {
        if (this.switches[userId]) {
            clearTimeout(this.switches[userId][chatId]);
            delete this.switches[userId][chatId];
        }
    }

    setStateSwitch({userId, chatId}, res, resultRouter) {
        return setTimeout(() =>
            this.startState({userId, chatId}, res.nextStateId, resultRouter), res.delayInMs);
    }

    async startState({userId, chatId, language}, stateId, resultRouter) {
        log.debug(`starting state [${stateId}] for user [${userId}] in chat [${chatId}]...`);
        await this.stateService.setCurrentStateId({userId, chatId}, stateId);
        const stateModel = await this.stateService.getState(stateId);

        const res = await stateModel.onStart({userId, chatId});
        return this.onStateResult(res, {
            userId,
            chatId,
            language,
            currentStateId: stateId
        }, resultRouter);
    }

    async onEvent(event, resultRouter) {
        const {userId, chatId, language} = event;

        if (this.stateService.missingScenariosData()) {
            log.debug(`missing scenarios data.`);
            log.debug(`getting the no scenarios state...`);
            const state = this.stateService.getNoScenariosDataState();
            const res = await state.onEvent(event);
            log.debug(`response from the no scenarios state [${JSON.stringify(res)}]`);
            return this.onStateResult(res, {userId, chatId, language}, resultRouter)
        }

        log.debug(`the scenarios data is available.`)

        this.clearStateSwitches({userId, chatId});

        const currentStateId = await this.stateService.getCurrentStateId({userId, chatId});

        if (!currentStateId || event.message === 'start') {
            return this.startState({userId, chatId}, process.env.ONBOARDING_STATE, resultRouter);
        }

        const currentState = await this.stateService.getState(currentStateId, language);
        const res = await currentState.onEvent(event);
        return this.onStateResult(res, {userId, chatId, language, currentStateId}, resultRouter);
    }

    async onStateResult(res, {userId, chatId, language, currentStateId}, resultRouter) {
        if (!res) {
            return Promise.resolve();
        }
        if (!(res instanceof EventResult)) {
            throw new Error('Returned result from state is not an instance of EventResult!');
        }

        log.debug(`received result from the state, result type is [${res.type}]`);
        if (res.messageObj) {
            await resultRouter.sendAppropriateMessage({userId, chatId}, res.messageObj, res.choices);
        }

        if (res.type === EVENT_RESULT_TYPE.SWITCH_STATE) {
            if (!this.switches[userId]) {
                this.switches[userId] = {};
            }
            this.switches[userId][chatId] = this.setStateSwitch({userId, chatId}, res, resultRouter);
            return Promise.resolve();
        } else if (res.type === EVENT_RESULT_TYPE.SCENARIO_FINISHED) {
            const previousState = await this.userService.getPreviousStateData({userId, chatId});
            if (!previousState) {
                return Promise.resolve();
            }

            let stateId = previousState.stateId;
            if (!process.env.RETURN_TO_PREVIOUS_STATE_SCENARIO) {
                await this.userService.removePreviousStateData({userId, chatId});
            } else {
                stateId = process.env.RETURN_TO_PREVIOUS_STATE_SCENARIO;
            }
            return this.startState({userId, chatId, language}, stateId, resultRouter);
        }

        return Promise.resolve();
    }
}

module.exports = TraversalService;
