module.exports = {
    MESSENGER_PROFILE_API_URL: 'https://graph.facebook.com/v2.6/{{user}}?fields=first_name,last_name,profile_pic,locale,timezone,gender&access_token=',
    MESSENGER_GRAPH_API_URL: 'https://graph.facebook.com/me',
    MESSENGER_SEND_API_URL: 'https://graph.facebook.com/v2.6/me/messages'
}
