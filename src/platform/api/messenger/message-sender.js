const axios = require('axios');
const _ = require('lodash');

const log = require('../../../log/winston').log(__filename);
const {MESSENGER_SEND_API_URL} = require('./constants');

const {MessageRepresentation} = require('./message-representation');
const {URL_TYPE_ENUM} = require('../enums');

const {getTypingSpeed} = require('../../../platform/components/states/common-functions');

const MAX_ELEMENTS_IN_CAROUSEL = 10;

class MessageSender {
    constructor({}, actionsSender) {
        this.actionsSender = actionsSender
    }

    async sendGifMessage({userId, chatId}, url, buttons) {
        log.debug(`send gif message: ${url}`);
        const msg = MessageRepresentation.Url({userId, buttons}, {url, urlType: URL_TYPE_ENUM.GIF});
        await sendRequest(msg);
    }

    async sendPlainUrl({userId, chatId}, url, buttons) {
        log.debug(`send plain url: ${url}`);
        const msg = MessageRepresentation.Url({userId, buttons}, {url, urlType: URL_TYPE_ENUM.PLAIN_URL});
        await sendRequest(msg);
    }

    async sendVideoMessage({userId, chatId}, url, buttons) {
        log.debug(`send video: ${url}`);
        const msg = MessageRepresentation.Url({userId, buttons}, {url, urlType: URL_TYPE_ENUM.PLAIN_URL});
        await sendRequest(msg);
    }

    async sendImageUrl({userId, chatId}, url, buttons) {
        log.debug(`send image url: ${url}`);
        const msg = MessageRepresentation.Url({userId, buttons}, {url, urlType: URL_TYPE_ENUM.IMAGE});
        await sendRequest(msg);
    }

    async sendTextMessage({userId, chatId}, text, buttons) {
        const msg = MessageRepresentation.Text({userId, buttons}, text);
        const speed = getTypingSpeed(text);
        await this.actionsSender.typingOn(userId, speed);
        await sendRequest(msg);
    }

    async sendTextMessages({userId, chatId}, messages, buttons) {
        log.debug(`send text messages: ${JSON.stringify(messages)}`);

        for (let i = 0; i < messages.length; i++) {
            const message = messages[i];
            await this.sendTextMessage({userId, chatId}, message, buttons);
        }
    }

    async sendCarousel({userId, chatId}, elements, buttons) {
        log.debug(`send carousel [${JSON.stringify(elements)}]...`);
        const chunks = _.chunk(elements, MAX_ELEMENTS_IN_CAROUSEL);

        for (let i = 0; i < chunks.length; i++) {
            const msg = MessageRepresentation.Carousel({userId, buttons}, chunks[i]);
            await sendRequest(msg);
        }
    }

}

const sendRequest = (data) =>
    new Promise((resolve, reject) => {
        axios.post(MESSENGER_SEND_API_URL, data, {
            headers: {'Content-Type': 'application/json'},
            params: {access_token: process.env.MESSENGER_PAGE_ACCESS_TOKEN}
        }).then(res => {
            log.debug(`message [${JSON.stringify(data)}] sent successfully.`);
            return resolve(res);
        }).catch(err => {
            log.debug(`sending message [${JSON.stringify(data)}], error: ${JSON.stringify(err)}`);
            return reject(err);
        });
    });


module.exports = MessageSender;
