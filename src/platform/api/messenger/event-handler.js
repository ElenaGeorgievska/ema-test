const log = require('../../../log/winston').log(__filename);

const EventHandler = require('../event-handler');
const {Message} = require('../../models/message');

const takeControlKeywords = ["test"];

const isSticker = function (messagingBody) {
    return (
        messagingBody.message.attachments &&
        messagingBody.message.attachments[0].payload &&
        messagingBody.message.attachments[0].payload.sticker_id
    );
}

class MessengerEventHandler extends EventHandler {
    constructor(eventParser, resultRouter, actionsSender,
                {userService, chatService, stateTraversalService, emojis}) {
        super(
            eventParser,
            resultRouter,
            {userService, chatService, stateTraversalService, emojis}
        );

        this.actionsSender = actionsSender;
    }

    async handlePostbackEvent(messagingBody) {
        const event = this.eventParser._parsePostback(messagingBody);
        const {userId} = event;

        await this.chatService.updateUserChat(event, event.message);
        await this.userService.updateUserLastActive(userId);
        await this.actionsSender.seen(userId);

        return await this._scheduleOnEvent(event);
    }

    async onMessageEvent(request) {
        for (let body of request.entry) {
            if (body.standby) {
                for (const event of body.standby) {
                    if (event.message.is_echo) {
                        const usersMessage = event.message.text.toLowerCase();
                        if (takeControlKeywords.indexOf(usersMessage) > -1) {
                            const userId = event.recipient.id;
                            await this.handoverProtocolService.takeControl(userId);
                        }
                    }
                }
                return undefined;
            }

            const messagingBody = body.messaging[0];
            if (messagingBody.message && messagingBody.message.is_echo) return;

            if (messagingBody.postback) {
                return this.handlePostbackEvent(messagingBody);
            } else if (messagingBody.message.text) {
                const shouldBlockTheEvent = await this._shouldBlockTheEvent(messagingBody.sender.id, messagingBody.message.text);
                if (shouldBlockTheEvent) return undefined;
            }

            const event = this.eventParser.parse(messagingBody);
            const {userId, chatId} = event;

            if (!event.hidden) {
                const userMsgObject = Message.UserMessage(event);
                await this.chatService.updateMessageFeed({userId, chatId}, userMsgObject);
                await this.chatService.updateUserChat({userId, chatId}, event);
            }

            await this.userService.updateUserLastActive(userId);
            await this.actionsSender.seen(userId);

            if (this.emojis.isEmoji(event.message) || isSticker(messagingBody)) {
                return this._sendEmoji({userId, chatId});
            }

            return await this._scheduleOnEvent(event);
        }
    }

    async onVerifyWebhookEvent(req, res) {
        const mode = req.query['hub.mode'];
        const token = req.query['hub.verify_token'];
        const challenge = req.query['hub.challenge'];

        if (mode === 'subscribe' &&
            token === process.env.MESSENGER_VERIFY_TOKEN) {
            log.debug('messenger webhook successfully verified');
            return res.status(200).send(challenge);
        }

        log.error('messenger webhook not verified!');
        res.sendStatus(403);
    }
}

module.exports = MessengerEventHandler;
