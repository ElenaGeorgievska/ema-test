const {URL_TYPE_ENUM} = require('../enums');

class MessageRepresentation {
    constructor() {
    }

    static Carousel({userId, buttons}, elements) {
        return {
            recipient: {id: userId},
            message: {
                attachment: {
                    type: 'template',
                    payload: {
                        elements,
                        template_type: 'generic'
                    }
                }
            }
        };
    }

    static Text({userId, buttons}, textMsg) {
        const json = {
            message: {
                text: textMsg
            },
            recipient: {
                id: userId
            },
            messaging_type: 'RESPONSE'
        }

        if (buttons) {
            json.message.quick_replies = buttons;
        }

        return json;
    }

    static Url({userId, buttons}, {url, urlType, og}) {
        const json = {
            recipient: {
                id: userId
            }
        };
        if (urlType === URL_TYPE_ENUM.GIF || urlType === URL_TYPE_ENUM.IMAGE) {
            json.message = {
                attachment: {
                    type: 'image',
                    payload: {
                        url,
                        is_reusable: true
                    }
                }
            }
        } else if (urlType === URL_TYPE_ENUM.PLAIN_URL) {
            json.message = {
                attachment: {
                    type: 'template',
                    payload: {
                        template_type: 'open_graph',
                        elements: [{
                            url
                        }]
                    }
                }
            }
        }

        if (buttons) {
            json.message.quick_replies = buttons;
        }
        return json;
    }

    static User({userId, chatId, timestamp, buttons}, {type, hidden, link, source, value, message}) {
    }
}

module.exports = {MessageRepresentation};
