const log = require('../../../log/winston').log(__filename);

const {Event} = require('../../models/event');

class EventParser {
    constructor() {
    }

    _parsePostback(body) {
        const userId = body.sender.id;
        const message = body.postback.title;
        const payload = JSON.parse(body.postback.payload);
        const value = payload.value;
        const link = payload.link;
        const type = 'action';
        const source = 'messenger';

        const parsed = new Event({
            userId, message, link, type, value, source
        });

        log.debug(`body successfully parsed.`);
        log.debug(`parsed body: ${JSON.stringify(parsed)}`);

        return parsed;
    }

    parse(body) {
        log.debug(`parsing body... [${JSON.stringify(body)}]`);

        if (body.postback) {
            return this._parsePostback(body);
        }

        const userId = body.sender.id;
        const message = body.message.text;
        const source = 'messenger';

        let type = 'message';
        let value = message;
        let link;
        if (body.message.quick_reply) {
            const payload = JSON.parse(body.message.quick_reply.payload);
            value = payload.value;
            link = payload.link;
            type = 'action';
        }

        const parsed = new Event({
            userId, message, link, type, value, source
        });

        log.debug(`body successfully parsed.`);
        log.debug(`parsed body: ${JSON.stringify(parsed)}`);

        return parsed;
    }
}

const getValue = function (quickReply, message) {
    if (!quickReply) return message;
    if (!quickReply.payload) return message;

    let value = message;
    if (quickReply.payload.includes(':::')) {
        const parts = quickReply.payload.split(':::');
        value = parts[1];
    }

    return value;
};

const getLink = function (quickReply) {
    if (!quickReply) return '';
    if (!quickReply.payload) return '';

    let link = quickReply;
    if (quickReply.payload.includes(':::')) {
        const parts = quickReply.payload.split(':::');
        link = parts[0];
    }

    return link;
};

module.exports = EventParser;
