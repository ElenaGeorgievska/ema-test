const axios = require('axios');

const log = require('../../../log/winston').log(__filename);

const {MESSENGER_SEND_API_URL} = require('./constants');

const accessToken = process.env.MESSENGER_PAGE_ACCESS_TOKEN;
const sendApiUrl = MESSENGER_SEND_API_URL;

class ActionsSender {
    constructor() {
    }

    async seen(userId) {
        await this._sendAction(userId, 'mark_seen');
    }

    async typingOn(userId, typingDelayInMs) {
        await this._sendAction(userId, 'typing_on', typingDelayInMs);
    }

    _sendAction(userId, actionType, typingDelayInMs = undefined) {
        return new Promise(resolve => {
            axios.post(sendApiUrl, {
                sender_action: actionType,
                recipient: {
                    id: userId
                }
            }, {
                headers: {'Content-Type': 'application/json'},
                params: {access_token: accessToken}
            }).then((res) => {
                if (res.status !== 200) {
                    log.error(`sending action error: ${JSON.stringify(res.data)}`);
                }
                setTimeout(() => resolve(), typingDelayInMs);
            }).catch((err) => log.error(`sending action error: ${JSON.stringify(err)}`));
        });
    }

}

module.exports = ActionsSender;
