class ResultRouter {
    constructor({chatService}, messageSender) {
        this.chatService = chatService;
        this.messageSender = messageSender;
    }
}

module.exports = ResultRouter;
