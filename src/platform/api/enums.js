const MESSAGE_TYPE_ENUM = {
    URL: 'URL',
    CAROUSEL: 'CAROUSEL',
    TEXT: 'TEXT',
    USER: 'USER',
};

const INPUT_MESSAGE_TYPE_ENUM = {
    action: 'action',
    message: 'message'
};

const URL_TYPE_ENUM = {
    PLAIN_URL: 'PLAIN_URL',
    VIDEO: 'VIDEO',
    IMAGE: 'IMAGE',
    GIF: 'GIF',
};

module.exports = {MESSAGE_TYPE_ENUM, INPUT_MESSAGE_TYPE_ENUM, URL_TYPE_ENUM};
