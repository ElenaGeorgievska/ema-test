const {URL_TYPE_ENUM} = require('../enums');

const CAROUSEL_SUBTITLE_MAX_CHARS = 170;
const CAROUSEL_IMAGE_DEFAULT = '';

const getRealUserId = (userId) => userId.split('K0SaCrTA').join('/');

class MessageRepresentation {
    constructor() {
    }

    static Text({userId, buttons}, textMsg) {
        const msg = {
            auth_token: process.env.VIBER_ACCESS_TOKEN,
            receiver: userId,
            type: 'text',
            text: textMsg
        };
        if (buttons) {
            const keyboard = getKeyboard(buttons);
            if (keyboard) {
                msg.keyboard = keyboard;
            }
        }

        return msg;
    }

    static Url({userId, buttons}, {url, urlType}) {
        const msg = {
            auth_token: process.env.VIBER_ACCESS_TOKEN,
            receiver: userId,
            type: 'url',
            media: url
        };

        if (urlType === URL_TYPE_ENUM.VIDEO) {
            msg.type = 'video';
        } else if (urlType === URL_TYPE_ENUM.IMAGE) {
            msg.type = 'picture';
        }

        if (buttons) {
            const keyboard = getKeyboard(buttons);
            if (keyboard) {
                msg.keyboard = keyboard;
            }
        }

        return msg;
    }

    static Carousel({userId, buttons}, elements) {
        if (!elements) return undefined;

        if (elements[0].buttons) {
            if (elements[0].buttons.length > 1) {
                return this._CarouselWithButtons({userId, buttons}, elements);
            }
            return this._CarouselWithOneButton({userId, buttons}, elements);
        }

        return this._CarouselWithoutButtons({userId, buttons}, elements);
    }

    static _CarouselWithButtons({userId, buttons}, elements) {
        const carouselElements = [];
        for (const element of elements) {
            this._addImageElement(carouselElements, element, 2);
            this._addTitleElement(carouselElements, element);
            this._addSubtitleElement(carouselElements, element, 2);
            this._addButtons(carouselElements, element);
        }

        return this._generateCarousel(userId, carouselElements);
    }

    static _CarouselWithOneButton({userId, buttons}, elements) {
        const carouselElements = [];
        for (const element of elements) {
            this._addImageElement(carouselElements, element, 3);
            this._addTitleElement(carouselElements, element);
            this._addSubtitleElement(carouselElements, element, 2);
            this._addOneButton(carouselElements, element);
        }

        return this._generateCarousel(userId, carouselElements);
    }

    static _CarouselWithoutButtons({userId, buttons}, elements) {
        const carouselElements = [];
        for (const element of elements) {
            this._addImageElement(carouselElements, element, 3);
            this._addTitleElement(carouselElements, element);
            this._addSubtitleElement(carouselElements, element, 3);
        }

        return this._generateCarousel(userId, carouselElements);
    }

    static _addImageElement(carouselElements, element, rows) {
        let image = CAROUSEL_IMAGE_DEFAULT;

        if (element.image_url) {
            image = element.image_url;
        }

        carouselElements.push({
            Columns: 6,
            Rows: rows,
            Silent: true,
            ActionType: 'none',
            Image: image
        });
    }

    static _addTitleElement(carouselElements, element) {
        carouselElements.push({
            Columns: 6,
            Rows: 1,
            Text: `<b><font color=#000000>${element.title}</font></b>`,
            TextSize: 'medium',
            TextVAlign: 'top',
            TextHAlign: 'left',
            ActionType: 'none'
        });
    }

    static _addSubtitleElement(carouselElements, element, rows) {
        const subtitle = element.subtitle.substring(0, CAROUSEL_SUBTITLE_MAX_CHARS);

        carouselElements.push({
            Columns: 6,
            Rows: rows,
            Text: `<font color=#666666>${subtitle}</font>`,
            TextSize: 'small',
            TextVAlign: 'top',
            TextHAlign: 'left',
            ActionType: 'none'
        });
    }

    static _addButtons(carouselElements, element) {
        const buttons = element.buttons;
        carouselElements.push({
            Columns: 6,
            Rows: 1,
            ActionType: 'reply',
            ActionBody: buttons[0].payload,
            Text: `<font color=#000000>${buttons[0].title}</font>`,
            TextSize: 'large',
            TextVAlign: 'middle',
            TextHAlign: 'middle',
            Image: 'https://s14.postimg.org/4mmt4rw1t/Button.png'
        })
        carouselElements.push({
            Columns: 6,
            Rows: 1,
            ActionType: 'reply',
            ActionBody: buttons[1].payload,
            Text: `<font color=#000000>${buttons[1].title}</font>`,
            TextSize: 'large',
            TextVAlign: 'middle',
            TextHAlign: 'middle',
            Image: 'https://s14.postimg.org/4mmt4rw1t/Button.png'
        })
    }

    static _addOneButton(carouselElements, element) {
        const button = element.buttons[0];
        if (button.type === 'web_url') {
            carouselElements.push({
                Columns: 6,
                Rows: 1,
                ActionType: 'open-url',
                ActionBody: button.url,
                Text: `<font color=#000000>${button.title}</font>`,
                TextSize: 'large',
                TextVAlign: 'middle',
                TextHAlign: 'middle',
                Image: 'https://s14.postimg.org/4mmt4rw1t/Button.png'
            })
        } else {
            carouselElements.push({
                Columns: 6,
                Rows: 1,
                ActionType: 'reply',
                ActionBody: button.payload,
                Text: `<font color=#000000>${button.title}</font>`,
                TextSize: 'large',
                TextVAlign: 'middle',
                TextHAlign: 'middle',
                Image: 'https://s14.postimg.org/4mmt4rw1t/Button.png'
            })
        }
    }

    static _generateCarousel(userId, carouselElements) {
        return {
            auth_token: process.env.VIBER_ACCESS_TOKEN,
            receiver: getRealUserId(userId),
            min_api_version: 2,
            type: 'rich_media',
            rich_media: {
                ButtonsGroupColumns: 6,
                ButtonsGroupRows: 7,
                BgColor: "#FFFFFF",
                Buttons: carouselElements,
                Type: 'rich_media'
            }
        };
    }
}

const getKeyboard = function (buttons) {
    buttons = formatButtons(buttons);
    if (!buttons) return undefined;

    return {
        Type: 'keyboard',
        DefaultHeight: true,
        Buttons: buttons,
        BgColor: '#F2F2F2'
    }
};


const hasAnyChoiceVisible = (buttons) => _.find(buttons, choice => !choice.hidden);
const getRandomButtonDescription = (descriptions) => descriptions[Math.floor(Math.random() * descriptions.length)];

const formatButtons = (choices) => {
    if (!choices) return undefined;
    if (!hasAnyChoiceVisible(choices)) return undefined;

    const quickReplies = [];

    choices.filter(choice => !choice.hidden)
        .forEach(choice => {
            const descriptions = choice.descriptions;
            let title = getRandomButtonDescription(descriptions);
            if (!title) {
                title = choice.description;
            }

            quickReplies.push({
                Columns: 3,
                Rows: 1,
                BgColor: '#FFFFFF',
                Text: `<b><font color=\'#666666\'>${title}</b></font>`,
                TextSize: 'regular',
                TextHAlign: 'center',
                TextVAlign: 'middle',
                TextShouldFit: true,
                ActionType: 'reply',
                ActionBody: JSON.stringify({link: choice.link, value: choice.value, message: title})
            })
        });

    if (quickReplies.length === 0) return;

    return quickReplies;
}

module.exports = {MessageRepresentation};
