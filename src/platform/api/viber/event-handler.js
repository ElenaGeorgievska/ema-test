const log = require('../../../log/winston').log(__filename);

const EventHandler = require('../event-handler');
const {Message} = require('../../models/message');


class ViberEventHandler extends EventHandler {
    constructor(eventParser, resultRouter,
                {userService, chatService, stateTraversalService, emojis}) {
        super(
            eventParser,
            resultRouter,
            {userService, chatService, stateTraversalService, emojis}
        );
    }

    async onMessageEvent(body, {userProfile}) {
        const bodyToParse = {
            body, userProfile
        };

        const shouldBlockTheEvent = await this._shouldBlockTheEvent(userProfile.id, body.text);
        if (shouldBlockTheEvent) return undefined;

        const event = this.eventParser.parse(bodyToParse);
        const {userId, chatId} = event;

        if (body.stickerId || this.emojis.isViberEmoji(event.message)) {
            const emoji = this.emojis.getRandomViberEmoji();
            return this._sendEmoji({userId, chatId}, emoji);
        }

        if (!body.text) {
            return undefined;
        }

        if (body.text.startsWith('http')) {
            return undefined;
        }

        if (!event.hidden) {
            const userMsgObject = Message.UserMessage(event);
            await this.chatService.updateMessageFeed({userId, chatId}, userMsgObject);

            await this.chatService.updateUserChat({userId, chatId}, event);
        }
        await this.userService.updateViberUserProfile(userId, userProfile);
        await this.userService.updateUserLastActive(userId);

        return await this._scheduleOnEvent(event);
    }
}

module.exports = ViberEventHandler;
