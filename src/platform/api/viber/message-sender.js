const axios = require('axios');

const log = require('../../../log/winston').log(__filename);

const {MessageRepresentation} = require('./message-representation');
const {URL_TYPE_ENUM} = require('../enums');

const VIBER_API_URL = 'https://chatapi.viber.com/pa/send_message';

const MAX_ELEMENTS_IN_CAROUSEL = 6;

class MessageSender {
    constructor() {
    }

    async sendGifMessage({userId, chatId}, url, buttons) {
        log.debug(`send gif message: [${url}]`);
        const msg = MessageRepresentation.Url({userId, buttons}, {url, urlType: URL_TYPE_ENUM.GIF});
        await sendRequest(msg);
    }

    async sendPlainUrl({userId, chatId}, url, buttons) {
        log.debug(`send plain url: [${url}]`);
        const msg = MessageRepresentation.Url({userId, buttons}, {url, urlType: URL_TYPE_ENUM.PLAIN_URL});
        await sendRequest(msg);
    }

    async sendVideoMessage({userId, chatId}, url, buttons) {
        log.debug(`send video: [${url}]`);
        const msg = MessageRepresentation.Url({userId, buttons}, {url, urlType: URL_TYPE_ENUM.VIDEO});
        await sendRequest(msg);
    }

    async sendImageUrl({userId, chatId}, url, buttons) {
        log.debug(`send image: [${url}]`);
        const msg = MessageRepresentation.Url({userId, buttons}, {url, urlType: URL_TYPE_ENUM.IMAGE});
        await sendRequest(msg);
    }

    async sendTextMessage({userId, chatId}, text, buttons) {
        const msg = MessageRepresentation.Text({userId, buttons}, text);
        await sendRequest(msg);
    }

    async sendTextMessages({userId, chatId}, messages, buttons) {
        for (let i = 0; i < messages.length; i++) {
            const message = messages[i];
            await this.sendTextMessage({userId, chatId}, message, buttons);
        }
    }

    async sendCarousel({userId, chatId}, elements, buttons) {
        log.debug(`send carousel [${JSON.stringify(elements)}]...`);

        const chunks = _.chunk(elements, MAX_ELEMENTS_IN_CAROUSEL);

        for (let i = 0; i < chunks.length; i++) {
            const msg = MessageRepresentation.Carousel({userId, buttons}, chunks[i]);
            await sendRequest(msg);
        }
    }
}

const sendRequest = (data) =>
    new Promise((resolve, reject) => {
        axios.post(VIBER_API_URL, data, {
            headers: {'Content-Type': 'application/json'}
        }).then(res => {
            log.debug(`message [${JSON.stringify(data, null, 3)}] sent successfully.`);
            return resolve(res);
        }).catch(err => {
            log.debug(`sending message [${JSON.stringify(data)}], error: ${JSON.stringify(err)}`);
            return reject(err);
        });
    });


module.exports = MessageSender;
