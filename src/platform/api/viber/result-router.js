const _ = require('lodash');
const log = require('../../../log/winston').log(__filename);

const {MSG_TYPE} = require('../../models/message');
const {URL_MSG_TYPE} = require('../../models/url-message');

const ResultRouter = require('../result-router');

class MessengerResultRouter extends ResultRouter {
    constructor({chatService}, messageSender) {
        super({chatService}, messageSender);
    }

    async _sendUrlMessage({userId, chatId}, urlObj, buttons) {
        switch (urlObj.urlType) {
            case URL_MSG_TYPE.GIF:
                return this.messageSender.sendGifMessage({userId, chatId}, urlObj.url, buttons);
            case URL_MSG_TYPE.PLAIN_URL:
                return this.messageSender.sendPlainUrl({userId, chatId}, urlObj.url, buttons);
            case URL_MSG_TYPE.VIDEO:
                return this.messageSender.sendVideoMessage({userId, chatId}, urlObj.url, buttons);
            case URL_MSG_TYPE.IMAGE:
                return this.messageSender.sendImageUrl({userId, chatId}, urlObj.url);
        }
    }

    async sendAppropriateMessage({userId, chatId}, messageObj) {
        await this._sendTypingOnAction(userId);
        await this.chatService.updateMessageFeed({userId, chatId}, messageObj);

        let {choices} = messageObj;
        choices = formatButtons(choices);

        log.debug(`should send message: ${JSON.stringify(messageObj)}`);

        switch (messageObj.type) {
            case MSG_TYPE.TEXT:
                return this.messageSender.sendTextMessages({userId, chatId}, messageObj.messages, choices);
            case MSG_TYPE.CAROUSEL:
                return this.messageSender.sendCarousel({userId, chatId}, messageObj.carouselElements, choices);
            case MSG_TYPE.URL:
                return this._sendUrlMessage({userId, chatId}, messageObj.urlObj, choices);
        }
    }

    async _sendTypingOnAction(userId, typingDelayInMs) {
    }
}

const hasAnyChoiceVisible = (buttons) => _.find(buttons, choice => !choice.hidden);
const getRandomButtonDescription = (descriptions) => descriptions[Math.floor(Math.random() * descriptions.length)];

const formatButtons = (choices) => {
    if (!choices) return undefined;
    if (!hasAnyChoiceVisible(choices)) return undefined;

    const quickReplies = [];

    choices.filter(choice => !choice.hidden)
        .forEach(choice => {
            const descriptions = choice.descriptions;
            let title = getRandomButtonDescription(descriptions);
            if (!title) {
                title = choice.description;
            }

            quickReplies.push({
                'min_api_version': 3,
                'Columns': 6,
                'Rows': 1,
                'BgColor': '#2db9b9',
                'Text': title,
                'Silent': true,
                'textOpacity': 70,
                'TextSize': 'regular',
                'TextHAlign': 'center',
                'TextVAlign': 'middle',
                'ActionType': 'reply',
                'ActionBody': JSON.stringify({link: choice.link, value: choice.value})
            })
        });

    if (quickReplies.length === 0) return;

    return quickReplies;
}


module.exports = MessengerResultRouter;
