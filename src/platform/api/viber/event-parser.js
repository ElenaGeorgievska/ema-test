const log = require('../../../log/winston').log(__filename);

const {Event} = require('../../models/event');

const updateUserId = (userId) => userId.split('/').join('K0SaCrTA');

class EventParser {
    constructor() {
    }

    parse(bodyToParse) {
        log.debug(`parsing body... [${JSON.stringify(bodyToParse)}]`);

        const userId = updateUserId(bodyToParse.userProfile.id);
        const message = bodyToParse.body.text;
        const source = 'viber';

        let type = 'message';
        let value = message;
        let link = '';

        let parsedMessage;
        try {
            parsedMessage = JSON.parse(message);
            console.log(`typeof message: ${typeof parsedMessage}`)

            if (typeof parsedMessage === 'object') {
                type = 'action';
                link = parsedMessage.link;
                value = parsedMessage.value;
            }
        } catch (e) {
            console.log(`not a button click event: ${e}`);
            type = 'message';
        }

        const parsed = new Event({
            userId, message, link, type, value, source
        });

        log.debug(`body successfully parsed.`);
        log.debug(`parsed body: ${JSON.stringify(parsed)}`);

        return parsed;
    }
}

module.exports = EventParser;
