const log = require('../../log/winston').log(__filename);

const MessengerResultRouter = require('./messenger/result-router');
const MessengerEventParser = require('./messenger/event-parser');
const MessengerEventHandler = require('./messenger/event-handler');
const MessengerMessageSender = require('./messenger/message-sender');
const ActionsSender = require('./messenger/actions-sender');

const ViberResultRouter = require('./viber/result-router');
const ViberEventParser = require('./viber/event-parser');
const ViberEventHandler = require('./viber/event-handler');
const ViberMessageSender = require('./viber/message-sender');

const WebResultRouter = require('./web/result-router');
const WebEventHandler = require('./web/event-handler');
const WebEventParser = require('./web/event-parser');
const WebMessageSender = require('./web/message-sender');

const graphqlSchema = require('./web/graphql/schema');
const initResolvers = require('./web/graphql/resolvers/init');
module.exports = function (components) {
    log.info(`* initializing the event handlers...`);
    const msnActionsSender = new ActionsSender();
    const msnEventParser = new MessengerEventParser();
    const msnMessageSender = new MessengerMessageSender(components, msnActionsSender);
    const msnResultRouter = new MessengerResultRouter(components, msnMessageSender, msnActionsSender);

    const viberEventParser = new ViberEventParser();
    const viberMessageSender = new ViberMessageSender(components);
    const viberResultRouter = new ViberResultRouter(components, viberMessageSender);

    const webEventParser = new WebEventParser();
    const webMessageSender = new WebMessageSender(components);
    const webResultRouter = new WebResultRouter(components, webMessageSender);

    log.info(`* event handlers initialized\n`);
    const webEventHandler = new WebEventHandler(webEventParser, webResultRouter, components)
    return {
        web: {
            graphqlSchema,
            graphqlResolvers: initResolvers(webEventHandler, components),
            eventHandler: webEventHandler,
        },
        messenger: {
            eventHandler: new MessengerEventHandler(
                msnEventParser, msnResultRouter, msnActionsSender, components
            )
        },
        viber: {
            eventHandler: new ViberEventHandler(
                viberEventParser, viberResultRouter, components
            )
        }
    };
};
