const {gql} = require('apollo-server-express');

const MessageType = gql`
    enum MessageType {
        "This type is used when the message contains a URL (plain url, image, video or GIF)"
        URL
        """
        This type is used when the message contains items that should be shown in a carousel.
        """
        CAROUSEL
        "This type is used for plain text messages that are sent by the bot."
        TEXT
        "This type is used for messages that are sent by the user (events)."
        USER
    }
`;

const InputMessageType = gql`
    "Used for user's message data"
    enum InputMessageType {
        """
        This type is used for sending 'button clicked' and 'carousel item selected' events.
        """
        action
        "This type is used for sending a plain text message."
        message
    }
`;

const UrlType = gql`
    "Bot's URL Message."
    enum UrlType {
        "This type is used for sending a URL from Video."
        VIDEO
        "This type is used for sending a URL from Image."
        IMAGE
        "This type is used for sending a URL from GIF."
        GIF
        "This type is used for sending plain URL message."
        PLAIN_URL
    }
`;

const OgMetadata = gql`
    "OG Metadata for the URL messages."
    type OgMetadata {
        "og-title"
        title: String
        "og-image"
        image: String
        "og-description"
        description: String
    }
`;

const UrlMsg = gql`
    type UrlMsg {
        url: String!
        "The specific URL type (VIDEO, IMAGE, GIF, PLAIN_URL)"
        urlType: UrlType!,
        "OG Metadata for the URL messages."
        og: OgMetadata
    }
`;

const CarouselPayloadButton = gql`
    type CarouselPayloadButton {
        "Used for the carousel button's title."
        title: String!
        "Always 'payload' for now."
        type: String!
        """
        When the item is selected, this should be sent as a 'value' in the
        InputMessage (argument of the 'sendMessage' mutation).
        """
        payload: String!
    }
`;

const CarouselElement = gql`
    type CarouselElement {
        "Carousel element's title."
        title: String!
        "Carousel element's description."
        description: String!
        "Carousel element's image. Can be empty. Should add a placeholder image when this is empty."
        imageUrl: String!
        "Carousel button. Defines what happens on user's choice."
        button: CarouselPayloadButton
    }
`;
const CarouselMsg = gql`
    "Contains the multiple items to be rendered in a carousel. Each item can be selectable by the user."
    type CarouselMsg {
        elements: [CarouselElement]!
    }
`;
const UserMsg = gql`
    "This is how the InputMessage is added in the message history."
    type UserMsg {
        """
        This is where the button's link is placed when sending a click button event.
        Can be empty when sending text messages and selecting carousel items.
        """
        link: String!
        """
        Message type. For 'button click' and 'carousel item selected' events,
        type is 'action'. For 'plain text' events this is 'message'.
        """
        type: InputMessageType!
        """
        This will be true if the message shouldn't be
        shown/rendered to the user.
        """
        hidden: Boolean!
        "The source is always 'web' for web interfaces"
        source:String!
        """
        This is where the entered text message is placed when sending text messages,
        and the clicked button's description when sending 'button clicked' events.
        Can be an empty string when sending a 'carousel item selected' event.
        """
        message: String!
        """
        This is an empty string for 'text message' events.
        For 'button clicked' events, it will be the selected button's value.
        For 'select carousel item' events, it contains the 'payload' value of the item.
        """
        value: String!
    }
`;

const MessagesResult = gql`    
    type MessagesResult {
        "The message feed."
        feed: [Message!]
        "The cursor used for pagination."
        cursor: String
    }`;


const Message = gql`
    type Message {
        "The ID of the user, or 'bot'."
        sender: String!
        "The ID of the chat this message belongs to."
        chatId: String
        "The message timestamp."
        timestamp: String!
        """
        An array of buttons that should be shown to the user.
        This will be an empty array if no buttons should be shown.
        The buttons displayed depend on the user's current (flow) state.
        """
        buttons: [Button]
        "The message type: can be URL, CAROUSEL, TEXT, USER."
        type: MessageType
        "When 'type' is CAROUSEL, this field is populated."
        carousel: CarouselMsg
        """
        When 'type' is TEXT, this field is populated with the
        text message.
        """
        text: String
        "When 'type' is URL, this field is populated."
        url: UrlMsg
        "When the type is USER, this field is populated."
        inputMessage: UserMsg
    }`;

const InputMessage = gql`
    "User's message (sent to the server)"
    input InputMessage {
        "User's ID"
        sender: String!
        "The ID of the chat this message belongs to."
        chatId: String
        """
        The value is:
        - the entered text message when sending text messages.
        - the clicked button's description when sending 'button clicked' event.
        - an empty string when sending 'selected carousel item' event.
        """
        message: String!
        "The source is always 'web' for web interfaces"
        source: String!
        """
        'action' when sending a 'button clicked' or 'selected carousel item' events,
        'message' when sending plain text messages.
        """
        type: InputMessageType!
        """
        This is used when sending a 'button clicked' event.
        Should be populated with the value from a Button.link received from the server.
        Send an empty string when sending text messages, or 'selected carousel item' events.
        """
        buttonLink: String!
        """
        Set this to true when sending an event that shouldn't be shown to the user.
        For example when you need to start a specific scenario (conversation flow) -
        the onbording flow, the help flow, etc.
        Or for events originating outside of the chat (user has trouble populating their bank statement,
        let the assistant pop up and ask 'Do you need assistance?)'
        """
        hidden: Boolean!
        """
        'value' is an empty string for text messages. On 'button click' events populate with Button.value.
        On 'carousel item selected' events, populate with CarouselItem.Button.payload.
        """
        value: String!
    }`;

const SendMessageResult = gql`
    """
    Returns true if the action succeeded.
    On failure returns false accompanied by an error.
    """
    type SendMessageResult {
        result: Boolean!
        error: String
    }`;

const MessageFeedPreviews = gql`
    """
    Returns message feed previews
    """
    type MessageFeedPreviews {
        message: String!
        chatId: String!
        userId: String!
        timestamp: String!
    }`;

module.exports = {
    MessageType,
    UrlType,
    OgMetadata,
    CarouselElement,
    CarouselMsg,
    CarouselPayloadButton,
    UrlMsg,
    UserMsg,
    Message,
    MessagesResult,
    InputMessage,
    InputMessageType,
    SendMessageResult,
    MessageFeedPreviews
};
