const {gql} = require("apollo-server-express");

const Button = gql`
    "The current state buttons that are shown to the user."
    type Button {
        "Button's ID."
        id: String
        "The title of the button."
        description: String
        "The button's value."
        value: String
        "The button's link (connecting the next state)"
        link: String
    }
`;

module.exports = {Button};
