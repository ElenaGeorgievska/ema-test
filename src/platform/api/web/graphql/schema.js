const {gql} = require("apollo-server-express");

const {
    MessageType,
    UrlType,
    OgMetadata,
    CarouselElement,
    CarouselMsg,
    CarouselPayloadButton,
    UrlMsg,
    UserMsg,
    Message,
    MessagesResult,
    InputMessage,
    InputMessageType,
    SendMessageResult,
    MessageFeedPreviews
} = require('./types/message');
const {Button} = require('./types/state-buttons');

// A schema is a collection of type definitions (hence "typeDefs")
// that together define the "shape" of queries that are executed against
// your data.
const schema = gql`

    # State Buttons
    ${Button}

    # Messages
    ${MessageType}
    ${UrlType}
    ${OgMetadata}
    ${CarouselElement}
    ${CarouselMsg}
    ${CarouselPayloadButton}
    ${UrlMsg}
    ${UserMsg}
    ${Message}
    ${MessagesResult}
    ${InputMessage}
    ${InputMessageType}
    ${SendMessageResult}
    ${MessageFeedPreviews}

    extend type Subscription {
        """
        Subscribe to a user's message stream.
        After a user sends a message to a server, the replies will come in this subscription.
        """
        messages(
            "User's ID"
            userId: String!
            "Chat ID this message belongs to."
            chatId: String
        ): Message
    }

    extend type Query {
        """
        Get user's message history.
        Should be called when loading up the chat window.
        After the initial load, you should use the subscription "messages" to listen for incoming communication.
        """
        messages(
            "ID of the user to retrieve the messages for."
            userId: String!
            "The Chat ID this message belongs to."
            chatId: String
            "Limit for the number of messages."
            limit: Int
            "The cursor used for pagination. If undefined returns the latest messages."
            cursor: String
        ): MessagesResult!

        """
        Query message feed previews.
        Should be called when showing a list of user's chats.
        The result is a object containing a string and a timestamp.
        """
        messageFeedPreviews(
            "ID of the user to get the chats previews for."
            userId: String!
        ): [MessageFeedPreviews]

        """
        Create new chat.
        Should be called when starting new chat.
        The result is a string of the chatId.
        """
        createChat(
            "ID of the user to generate chat ID for."
            userId: String!
        ): String!
    }

    extend type Mutation {
        """
        Used for sending 'text messages',
        'button clicked' events and
        'select carousel item' events
        """
        sendMessage(
            "This is a message object that is sent by a user."
            inputMessage: InputMessage
        ): SendMessageResult!
    }
`;
module.exports = schema;
