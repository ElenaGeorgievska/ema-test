const getQueryResolvers = (components) => {
    return ({
        createChat: (_, {userId}) =>
            components.chatService.createNewChat(),
        messages: async (root, {userId, chatId, limit, cursor}, context) =>
            components.chatService.getMessageFeed({userId, chatId}, limit, cursor),
        messageFeedPreviews: async (root, {userId}, context) =>
            components.chatService.getMessageFeedPreviews(userId)
    })
};
module.exports = getQueryResolvers;
