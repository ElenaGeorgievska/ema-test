module.exports = (webEventHandler, components) => ({
    sendMessage(root, {inputMessage}, context) {
        console.log('send message called in mutation: ', inputMessage);
        const {sender, chatId, buttonLink, hidden, message, value, source, type} = inputMessage;

        return webEventHandler.onMessageEvent({
            hidden,
            message,
            value,
            source,
            type,
            chatId,
            user: sender,
            action: buttonLink,
        }).then(res => ({result: true})).catch(err => ({result: false, error: err}));
    }
});
