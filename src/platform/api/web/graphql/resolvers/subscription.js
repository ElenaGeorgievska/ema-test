const {MESSAGES_TOPIC} = require('../topics');
const {withFilter} = require('apollo-server-express');
module.exports = (components) => ({
    // listen to new messages
    messages: {
        subscribe: withFilter(
            () => components.pubSub.asyncIterator(MESSAGES_TOPIC),
            (payload, variables) => {
                console.log(`in subscription for message: ${JSON.stringify(payload)}`);
                console.log(`sending ${payload.messages.type} message`);

                return variables.userId === payload.userId;
            }
        )
    },
});
