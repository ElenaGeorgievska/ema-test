const getQueryResolvers = require('./query');
const getMutationResolvers = require('./mutation');
const getSubscriptionResolvers = require('./subscription');

const getResolvers = (webEventHandler, components) => {
    return {
        Query: getQueryResolvers(components),
        Mutation: getMutationResolvers(webEventHandler, components),
        Subscription: getSubscriptionResolvers(components),
    }
};
module.exports = getResolvers;
