const _ = require('lodash');
const urlMetadata = require('url-metadata');

const {MSG_TYPE} = require('../../models/message');
const {URL_MSG_TYPE} = require('../../models/url-message');

const log = require('../../../log/winston').log(__filename);

const urlify = function (text) {
    let match = text.match(/(https?:\/\/[^\s]+)/g) || text.match(/(www.[^\s]+)/);

    if (!match) return {text};

    text = text.replace(match[0], '');

    return {
        text,
        url: match[0]
    };
};

function generateUrlMessage(metadata) {
    const message = {};
    message.author = _.defaultTo(metadata.author, '');
    message.url = _.defaultTo(metadata.url, '');
    message.description = _.defaultTo(metadata.description, '');
    message.image = _.defaultTo(metadata.image, '');
    message.title = _.defaultTo(metadata.title, '');
    message.sourceUrl = _.defaultTo(metadata.source, message.url);
    return message;
}

class MessageSenderFirebase {
    constructor(userService) {
        this.userService = userService;
    }

    async sendGifMessage({userId, chatId}, url) {
        log.debug(`send gif: ${url}`);

        const message = {
            conversations: [{url}],
            sender: 'bot',
            type: 'messageGif',
            timestamp: _.now()
        };

        await this.userService.updateUserChat({userId, chatId}, message);
    }

    async sendPlainUrl({userId, chatId}, url) {
        log.debug(`send plain url: ${url}`);
        await urlMetadata(url)
            .then(async (metadata) => {
                const message = generateUrlMessage(metadata);
                return this.userService.updateUserChat({userId, chatId}, message);
            }, err => log.error(`error: ${err}`));
    }

    async sendVideoMessage({userId, chatId}, url) {
        log.debug(`send video: ${url}`);
        await urlMetadata(url)
            .then(async (metadata) => {
                const message = generateUrlMessage(metadata);
                return this.userService.updateUserChat({userId, chatId}, message);
            }, err => log.error(`error: ${err}`));
    }

    async sendImageUrl({userId, chatId}, url) {
        log.debug(`send image: ${url}`);

        const message = {
            url,
            conversations: [{url}],
            sender: 'bot',
            type: 'messageImage',
            timestamp: _.now()
        };
        return this.userService.updateUserChat({userId, chatId}, message);
    }

    async sendTextMessage({userId, chatId}, text) {
        log.debug(`send text message: ${text}`);

        const message = {
            type: 'message',
            sender: 'bot',
            conversations: [{message: text}],
            timestamp: _.now()
        }

        return this.userService.updateUserChat({userId, chatId}, message);
    }

    async sendTextMessages({userId, chatId}, messages, buttons) {
        log.debug(`send text messages: ${JSON.stringify(messages)}`);

        let buttonsToSend;

        for (let i = 0; i < messages.length; i++) {
            const isLastMessage = i === messages.length - 1;
            const message = messages[i];

            if (isLastMessage) {
                buttonsToSend = buttons;
            }

            let result = await urlify(message);

            if (!result.url) {
                if (result.text && result.text.length > 0) {
                    await this.sendTextMessage({userId, chatId}, result.text);
                }
            } else {
                if (result.text && result.text.length > 0) {
                    await this.sendTextMessage({userId, chatId}, result.text);
                }

                await this.sendPlainUrl({userId, chatId}, result.url);
            }
        }
    }

    sendUrlMessage({userId, chatId}, urlObj) {
        switch (urlObj.urlType) {
            case URL_MSG_TYPE.GIF:
                return this.sendGifMessage({userId, chatId}, urlObj.url);
            case URL_MSG_TYPE.PLAIN_URL:
                return this.sendPlainUrl({userId, chatId}, urlObj.url);
            case URL_MSG_TYPE.VIDEO:
                return this.sendVideoMessage({userId, chatId}, urlObj.url);
            case URL_MSG_TYPE.IMAGE:
                return this.sendImageUrl({userId, chatId}, urlObj.url);
        }
    }

    async sendCarousel({userId, chatId}, elements) {
        log.debug(`sending carousel [${JSON.stringify(elements)}]...`);

        const message = {
            elements,
            sender: 'bot',
            timestamp: _.now(),
            type: 'carousel'
        };

        return this.userService.updateUserChat({userId, chatId}, message);
    }

    async sendAppropriateMessage({userId, chatId}, messageObj) {
        log.debug(`should send message: ${JSON.stringify(messageObj)}`);

        switch (messageObj.type) {
            case MSG_TYPE.URL:
                return this.sendUrlMessage({userId, chatId}, messageObj.urlObj);
            case MSG_TYPE.TEXT:
                return this.sendTextMessages({userId, chatId}, messageObj.messages);
            case MSG_TYPE.CAROUSEL:
                return this.sendCarousel({userId, chatId}, messageObj.carouselElements);
        }
    }
}

module.exports = MessageSenderFirebase;
