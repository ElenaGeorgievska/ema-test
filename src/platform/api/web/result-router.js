const ResultRouter = require('../result-router');
const log = require('../../../log/winston').log(__filename);

const {MSG_TYPE} = require('../../models/message');
const {URL_MSG_TYPE} = require('../../models/url-message');

class WebResultRouter extends ResultRouter {
    constructor({chatService}, messageSender) {
        super({chatService}, messageSender);
    }

    _sendUrlMessage({userId, chatId}, urlObj, buttons) {
        switch (urlObj.urlType) {
            case URL_MSG_TYPE.GIF:
                return this.messageSender.sendGifMessage({userId, chatId}, urlObj.url, buttons);
            case URL_MSG_TYPE.PLAIN_URL:
                return this.messageSender.sendPlainUrl({userId, chatId}, urlObj.url, buttons);
            case URL_MSG_TYPE.VIDEO:
                return this.messageSender.sendVideoMessage({userId, chatId}, urlObj.url, buttons);
            case URL_MSG_TYPE.IMAGE:
                return this.messageSender.sendImageUrl({userId, chatId}, urlObj.url, buttons);
        }
    }

    async sendAppropriateMessage({userId, chatId}, messageObj) {
        log.debug(`should send message: ${JSON.stringify(messageObj)}`);
        await this.chatService.updateMessageFeed({userId, chatId}, messageObj);

        let {choices} = messageObj;
        choices = formatButtons(choices);

        switch (messageObj.type) {
            case MSG_TYPE.TEXT:
                return this.messageSender.sendTextMessages({userId, chatId}, messageObj.messages, choices);
            case MSG_TYPE.CAROUSEL:
                return this.messageSender.sendCarousel({userId, chatId}, messageObj.carouselElements, choices);
            case MSG_TYPE.URL:
                return this._sendUrlMessage({userId, chatId}, messageObj.urlObj, choices);
        }
    }
}

const getRandomItem = (array) => array[Math.floor(Math.random() * array.length)];

const formatButtons = (stateButtons) => {
    if (!stateButtons) return [];

    const buttons = [];

    for (const choice of stateButtons) {
        let {descriptions, hidden, id, link, value} = choice;
        if (!hidden) {
            const description = getRandomItem(descriptions);
            if (!value) {
                value = "";
            }
            buttons.push({
                description, id, link, value
            });
        }
    }

    return buttons;
}


module.exports = WebResultRouter;
