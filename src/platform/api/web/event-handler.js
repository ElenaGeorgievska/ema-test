const EventHandler = require('../event-handler');
const {MESSAGES_TOPIC} = require('./graphql/topics');

const {MessageRepresentation} = require('./message-representation');
const {Message} = require('../../models/message');

class WebEventHandler extends EventHandler {
    constructor(eventParser, resultRouter, {pubSub, userService, chatService, stateTraversalService, emojis}) {
        super(
            eventParser,
            resultRouter,
            {userService, chatService, stateTraversalService, emojis}
        );

        this.pubSub = pubSub;
    }

    async onMessageEvent(body) {
        const shouldBlockTheEvent = await this._shouldBlockTheEvent(body.user, body.message);
        if (shouldBlockTheEvent) return undefined;

        const event = this.eventParser.parse(body);
        const {userId, chatId} = event;

        if (!event.hidden) {
            const eventKeys = Object.keys(event);
            eventKeys.forEach(key => {
                if (event[key] === undefined) {
                    event[key] = null;
                }
            });
            await this.chatService.updateUserChat({userId, chatId}, event);

            const userMsgObject = Message.UserMessage(event);
            await this.chatService.updateMessageFeed({userId, chatId}, userMsgObject);

            const graphQLUserMsg = this.parseEventToUserMsg(event);
            await this.pubSub.publish(MESSAGES_TOPIC, {
                messages: graphQLUserMsg,
                userId
            });
        }
        await this.userService.updateUserLastActive(userId);

        if (this.emojis.isEmoji(event.message)) {
            return this._sendEmoji({userId, chatId});
        }

        return await this._scheduleOnEvent(event);
    }

    parseEventToUserMsg(userMsg) {
        // TODO this should be done when the message is received, InputMessage should be
        // of this format
        if (!(userMsg instanceof MessageRepresentation)) {
            const {hidden, link, type, message, userId, chatId, source, timestamp, value} = userMsg;
            userMsg = MessageRepresentation.User({userId, chatId, timestamp, buttons: []}, {
                type,
                hidden,
                link,
                source,
                value,
                message
            });
            return userMsg;
        }
    }
}

module.exports = WebEventHandler;
