const _ = require('lodash');

const {URL_TYPE_ENUM, MESSAGE_TYPE_ENUM, INPUT_MESSAGE_TYPE_ENUM} = require('../enums');

const DEFAULT_CHAT_ID = 'default_chat_id';

const generateDefaultChatId = function (userId) {
    return userId + '_' + DEFAULT_CHAT_ID;
}

class CarouselPayloadButton {
    constructor({type, title, payload}) {
        this.type = type;
        this.title = title;
        this.payload = payload;
    }
}

class CarouselElement {
    constructor({title, description, imageUrl, button}) {
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
        this.button = button;
    }
}

class CarouselMsg {
    constructor(elements) {
        elements.forEach(elem => {
            if (!elem instanceof CarouselElement) {
                throw 'carousel items not instances of class CarouselElement'
            }
        });
        this.elements = elements;
    }
}

class OgMetadata {
    constructor(title, image, description) {
        if (title) {
            this.title = title;
        }
        if (image) {
            this.image = image;
        }
        if (description) {
            this.description = description;
        }
    }
}

class UrlMsg {
    constructor(url, urlType, og) {
        if (og && !(og instanceof OgMetadata)) {
            throw 'og metadata in UrlMsg is optional, but when provided must be instance of OgMetadata'
        }
        if (!URL_TYPE_ENUM[urlType]) {
            throw `url type ${urlType} not in ${JSON.stringify(URL_TYPE_ENUM)}`
        }
        this.url = url;
        this.urlType = urlType;
        if (og) {
            this.og = og;
        }
    }
}

class UserMsg {
    constructor(type, hidden, link, source, value, message) {
        if (!INPUT_MESSAGE_TYPE_ENUM[type]) {
            throw `input type ${type} not in ${JSON.stringify(INPUT_MESSAGE_TYPE_ENUM)}`;
        }
        this.type = type;
        this.hidden = hidden;
        this.link = link;
        this.source = source;
        this.message = message;
        this.value = value;
    }
}

class MessageRepresentation {
    constructor({sender, chatId, buttons, type}, {carouselMsg, textMsg, urlMsg, inputMessage}) {
        this.sender = _.defaultTo(sender, 'bot');
        this.chatId = _.defaultTo(chatId, generateDefaultChatId(chatId));
        this.buttons = buttons;
        this.type = type;
        this.timestamp = _.now();

        if (carouselMsg) {
            this.carousel = carouselMsg;
        }
        if (urlMsg) {
            this.url = urlMsg;
        }
        if (textMsg) {
            this.text = textMsg;
        }
        if (inputMessage) {
            this.inputMessage = inputMessage;
        }
    }

    static Carousel({chatId, buttons}, carouselElements) {
        const carouselMsg = new CarouselMsg(carouselElements);
        return new MessageRepresentation({
            chatId,
            buttons,
            type: MESSAGE_TYPE_ENUM.CAROUSEL
        }, {carouselMsg})
    }

    static Text({chatId, buttons}, textMsg) {
        return new MessageRepresentation({
            chatId,
            buttons,
            type: MESSAGE_TYPE_ENUM.TEXT
        }, {textMsg})
    }

    static Url({chatId, buttons}, {url, urlType, og}) {
        const urlMsg = new UrlMsg(url, urlType, og);
        return new MessageRepresentation({
            chatId,
            buttons,
            type: MESSAGE_TYPE_ENUM.URL
        }, {urlMsg});
    }

    static User({userId, chatId, timestamp, buttons}, {type, hidden, link, source, value, message}) {
        const inputMessage = new UserMsg(type, hidden, link, source, value, message);
        return new MessageRepresentation({
            chatId,
            timestamp,
            buttons,
            sender: userId,
            type: MESSAGE_TYPE_ENUM.USER,
        }, {inputMessage});
    }
}

module.exports = {MessageRepresentation, OgMetadata, CarouselElement, CarouselPayloadButton};
