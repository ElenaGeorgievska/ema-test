const _ = require('lodash');

const log = require('../../../log/winston').log(__filename);

const {Event} = require('../../models/event');

class EventParser {
    constructor() {
    }

    parsePostback(body) {
        log.debug(`parsing body... [${JSON.stringify(body)}]`);

        const userId = body.user;
        const message = body.message;
        const payload = JSON.parse(body.value);
        const value = payload.value;
        const link = payload.link;
        const type = 'action';
        const source = 'web';

        const parsed = new Event({
            userId, message, link, type, value, source
        });

        log.debug(`body successfully parsed.`);
        log.debug(`parsed body: ${JSON.stringify(parsed)}`);

        return parsed;
    }

    parse(body) {
        if (isJson(body.value)) {
            return this.parsePostback(body);
        }

        log.debug(`parsing body... [${JSON.stringify(body)}]`);

        const userId = body.user;
        const link = body.action;
        const value = _.defaultTo(body.value, '');

        const {chatId, message, isSilent, source, type} = body;

        const parsed = new Event({
            userId, chatId, message, link, type, value, isSilent, source
        });

        log.debug(`body successfully parsed.`);
        log.debug(`parsed body: ${JSON.stringify(parsed)}`);

        return parsed;
    }
}

const isJson = function (value) {
    try {
        JSON.parse(value);
        return true;
    } catch (e) {
        return false;
    }
}

module.exports = EventParser;
