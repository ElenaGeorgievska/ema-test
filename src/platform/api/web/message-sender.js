const _ = require('lodash');
const urlMetadata = require('url-metadata');


const log = require('../../../log/winston').log(__filename);

const {MESSAGES_TOPIC} = require('./graphql/topics');
const {MessageRepresentation, OgMetadata, CarouselElement, CarouselPayloadButton} = require('./message-representation');
const {URL_TYPE_ENUM} = require('../enums');

const urlify = function (text) {
    let match = text.match(/(https?:\/\/[^\s]+)/g) || text.match(/(www.[^\s]+)/);

    if (!match) return {text};

    text = text.replace(match[0], '');

    return {
        text,
        url: match[0]
    };
};

function generateUrlMessage(metadata) {
    const message = {};
    message.author = _.defaultTo(metadata.author, '');
    message.url = _.defaultTo(metadata.url, '');
    message.description = _.defaultTo(metadata.description, '');
    message.image = _.defaultTo(metadata.image, '');
    message.title = _.defaultTo(metadata.title, '');
    message.sourceUrl = _.defaultTo(metadata.source, message.url);
    return message;
}

class MessageSender {
    constructor({pubSub}) {
        this.pubSub = pubSub;
    }

    async sendGifMessage({userId, chatId}, url, buttons) {
        log.debug(`send gif: ${url}`);

        const msg = MessageRepresentation.Url({chatId, buttons}, {
            url,
            urlType: URL_TYPE_ENUM.GIF
        });

        return this.pubSub.publish(MESSAGES_TOPIC, {
            messages: msg,
            userId
        });
    }

    async sendPlainUrl({userId, chatId}, url, buttons) {
        log.debug(`send plain url: ${url}`);
        await urlMetadata(url)
            .then(async (metadata) => {
                const message = generateUrlMessage(metadata);
                const {url, title, description, image} = message;

                const og = new OgMetadata(title, image, description);

                const msg = MessageRepresentation.Url({chatId, buttons}, {
                    url,
                    og,
                    urlType: URL_TYPE_ENUM.PLAIN_URL
                });

                return this.pubSub.publish(MESSAGES_TOPIC, {
                    messages: msg,
                    userId
                });
            }, err => log.error(`error: ${err}`));
    }

    async sendVideoMessage({userId, chatId}, url, buttons) {
        log.debug(`send video: ${url}`);
        await urlMetadata(url)
            .then(async (metadata) => {
                const message = generateUrlMessage(metadata);

                const {url, title, image, description} = message;

                const og = new OgMetadata(title, image, description);

                const msg = MessageRepresentation.Url({chatId, buttons}, {
                    url,
                    og,
                    urlType: URL_TYPE_ENUM.VIDEO
                });

                return this.pubSub.publish(MESSAGES_TOPIC, {
                    messages: msg,
                    userId
                });
            }, err => log.error(`error: ${err}`));
    }

    async sendImageUrl({userId, chatId}, url, buttons) {
        log.debug(`send image: ${url}`);

        const msg = MessageRepresentation.Url({chatId, buttons}, {
            url,
            urlType: URL_TYPE_ENUM.IMAGE
        });

        return this.pubSub.publish(MESSAGES_TOPIC, {
            messages: msg,
            userId
        });
    }

    async sendTextMessage({userId, chatId}, text, buttons) {
        log.debug(`send text message: ${text}`);

        const msg = MessageRepresentation.Text({chatId, buttons}, text);

        return this.pubSub.publish(MESSAGES_TOPIC, {
            messages: msg,
            userId
        });
    }

    async sendTextMessages({userId, chatId}, messages, buttons) {
        log.debug(`send text messages: ${JSON.stringify(messages)}`);

        let buttonsToSend;

        for (let i = 0; i < messages.length; i++) {
            const isLastMessage = i === messages.length - 1;
            const message = messages[i];

            if (isLastMessage) {
                buttonsToSend = buttons;
            }

            let result = await urlify(message);

            if (!result.url) {
                if (result.text && result.text.length > 0) {
                    await this.sendTextMessage({userId, chatId}, result.text, buttonsToSend);
                }
            } else {
                if (result.text && result.text.length > 0) {
                    await this.sendTextMessage({userId, chatId}, result.text, buttonsToSend);
                }

                await this.sendPlainUrl({userId, chatId}, result.url, buttonsToSend);
            }
        }
    }

    async sendCarousel({userId, chatId}, elements, buttons) {
        log.debug(`sending carousel [${JSON.stringify(elements)}]...`);

        const carouselElements = [];

        for (const element of elements) {
            const {title, image_url, subtitle} = element;
            const carouselButtons = element.buttons;
            let carouselButton;
            if (carouselButtons) {
                const button = carouselButtons[0];
                carouselButton = new CarouselPayloadButton({
                    title: button.title,
                    type: button.type,
                    payload: button.payload
                });
            }

            const carouselElement = new CarouselElement({
                title,
                description: subtitle,
                imageUrl: image_url,
                button: carouselButton
            });
            carouselElements.push(carouselElement);
        }

        const msg = MessageRepresentation.Carousel({chatId, buttons}, carouselElements);

        return this.pubSub.publish(MESSAGES_TOPIC, {
            messages: msg,
            userId
        });
    }
}


module.exports = MessageSender;
