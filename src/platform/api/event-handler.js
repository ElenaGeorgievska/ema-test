const Debouncer = require('../components/debouncer');

const {Message} = require('../models/message');

const SECOND_IN_MILLIS = 1000;

class EventHandler {
    constructor(eventParser, resultRouter, {userService, chatService, stateTraversalService, emojis}) {
        this.eventParser = eventParser;
        this.resultRouter = resultRouter;

        this.userService = userService;
        this.chatService = chatService;
        this.stateTraversalService = stateTraversalService;

        this.emojis = emojis;

        this.userMsgDebouncer = new Debouncer(process.env.MESSAGE_EVENT_DEBOUNCE_MS);
    }

    async _scheduleOnEvent(event) {
        event.language = await this.userService.getUserLanguage(event.userId);

        this.userMsgDebouncer.scheduleDebounced(event.userId, event.message, (cachedMessages) => {
            event.message = cachedMessages.join(' ');
            this.stateTraversalService.onEvent(event, this.resultRouter);
        });
    }

    async _sendEmoji({userId, chatId}, emoji) {
        if (!emoji) {
            emoji = this.emojis.getRandomEmoji();
        }
        const messageObj = Message.TextMessages([emoji])
        await this.resultRouter.sendAppropriateMessage({userId, chatId}, messageObj);
    }

    async _shouldBlockTheEvent(userId, text) {
        const currentEvent = {
            text,
            timestamp: Date.now()
        }
        const lastEvent = await this.userService.getLastEvent(userId);
        if (lastEvent) {
            if ((Date.now() - lastEvent.timestamp <= 5 * SECOND_IN_MILLIS) &&
                (currentEvent.text === lastEvent.text)) {
                return true;
            }
        }
        if (currentEvent.text) {
            await this.userService.setLastEvent(userId, currentEvent);
        }
    }
}

module.exports = EventHandler;
