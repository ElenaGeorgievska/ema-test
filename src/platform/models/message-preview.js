const _ = require('lodash');
const {MSG_TYPE} = require('./message');
const {URL_TYPE_ENUM} = require('../api/enums');

const botName = _.defaultTo(process.env.BOT_NAME, 'The Bot');

class MessagePreview {
    constructor(type, userId, message, timestamp) {
        this.type = type;
        this.userId = userId;
        this.message = message;
        this.timestamp = timestamp;
    }

    static UserMessage(userId, messageObj) {
        const {message} = messageObj.messages;
        const {timestamp} = messageObj;
        return new MessagePreview(MSG_TYPE.USER, userId, message, timestamp)
    }

    static TextMessages(userId, messageObj) {
        const {messages, timestamp} = messageObj;
        const message = messages[messages.length - 1];
        return new MessagePreview(MSG_TYPE.TEXT, userId, message, timestamp);
    }

    static UrlMessage(userId, messageObj) {
        const {urlObj, timestamp} = messageObj;
        let message;
        switch (urlObj.urlType) {
            case URL_TYPE_ENUM.PLAIN_URL:
                message = `${botName}: ${urlObj.url}`;
                break;
            case URL_TYPE_ENUM.IMAGE:
                message = `${botName} sent an image`;
                break;
            case URL_TYPE_ENUM.GIF:
                message = `${botName} sent a GIF`;
                break;
            case URL_TYPE_ENUM.VIDEO:
                message = `${botName} sent a video`;
                break;
        }
        return new MessagePreview(MSG_TYPE.URL, userId, message, timestamp);
    }

    static CarouselElements(userId, messageObj) {
        const {carouselElements, timestamp} = messageObj;
        if (!carouselElements || carouselElements.length === 0) {
            return undefined;
        }
        let message = `${botName} has sent a carousel with elements: `;
        for (let i = 0; i < carouselElements.length; i++) {
            const element = carouselElements[i];
            if (i === 0) {
                message += element.title;
            } else {
                message += ', ' + element.title;
            }
        }
        return new MessagePreview(MSG_TYPE.CAROUSEL, userId, message, timestamp);
    }
}

module.exports = {MessagePreview};
