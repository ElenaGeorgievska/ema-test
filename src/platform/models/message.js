const _ = require('lodash');

const MSG_TYPE = {
    URL: 'URL',
    CAROUSEL: 'CAROUSEL',
    TEXT: 'TEXT',
    USER: 'USER',
};

class Message {
    // Should not be called directly.
    // Use Message.static methods
    constructor(type, {messages, text, urlObj, carouselElements}, choices) {
        this.type = type;
        this.messages = messages;
        this.text = text;
        this.urlObj = urlObj;
        this.carouselElements = carouselElements;
        this.timestamp = _.now() + '';

        if (choices) {
            const choice = _.find(choices, choice => !choice.hidden);
            if (choice) {
                this.choices = choices;
            }
        }

    }

    static UserMessage(messages) {
        return new Message(MSG_TYPE.USER, {messages})
    }

    static TextMessages(messages, choices) {
        return new Message(MSG_TYPE.TEXT, {messages}, choices);
    }

    static UrlMessage(urlObj, choices) {
        return new Message(MSG_TYPE.URL, {urlObj}, choices);
    }

    static CarouselElements(elements, choices) {
        return new Message(MSG_TYPE.CAROUSEL, {carouselElements: elements}, choices);
    }
}

module.exports = {Message, MSG_TYPE};
