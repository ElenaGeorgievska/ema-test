const URL_MSG_TYPE = {
    PLAIN_URL: 'PLAIN_URL',
    VIDEO: 'VIDEO',
    IMAGE: 'IMAGE',
    GIF: 'GIF',
};

class UrlMessage {
    constructor(urlType, url) {
        this.urlType = urlType;
        this.url = url;
    }
}

module.exports = {UrlMessage, URL_MSG_TYPE};
