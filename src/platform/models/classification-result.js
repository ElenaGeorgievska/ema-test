const {EVENT_RESULT_TYPE, EventResult} = require('./event-result');

const CLASSIFICATION_RESULT_TYPE = {
    ...EVENT_RESULT_TYPE,
    ACTION: 'action'
};

class ClassificationResult extends EventResult {
    constructor(type, {nextStateId, messageObj, event}) {
        super(type, {nextStateId, messageObj});
        this.event = event;
    }

    static Action(event) {
        return new ClassificationResult(CLASSIFICATION_RESULT_TYPE.ACTION, {event});
    }
}

module.exports = {ClassificationResult, CLASSIFICATION_RESULT_TYPE};
