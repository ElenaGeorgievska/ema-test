const _ = require('lodash');

const EVENT_TYPE = {
    ACTION: 'action',
    MESSAGE: 'message'
};

const DEFAULT_CHAT_ID = 'default_chat_id';

class Event {
    constructor({userId, chatId, message, link, type, value, isSilent, source}) {
        this.userId = userId;
        this.message = message;
        this.type = type;
        this.source = source;
        this.chatId = _.defaultTo(chatId, userId + '_' + DEFAULT_CHAT_ID);
        this.link = _.defaultTo(link, '');
        this.value = _.defaultTo(value, '');
        this.hidden = _.defaultTo(isSilent, false);
        this.timestamp = Date.now();
    }
}

module.exports = {Event, EVENT_TYPE};
