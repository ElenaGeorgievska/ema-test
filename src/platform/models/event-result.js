const EVENT_RESULT_TYPE = {
    SWITCH_STATE: 'switch_state',
    SCENARIO_FINISHED: 'scenario_finished',
    WAIT: 'wait',
};

const {MSG_TYPE} = require('./message');

class EventResult {
    // Should not be called directly.
    // Use EventResult.static methods
    constructor(type, {messageObj, nextStateId, delayInMs}) {
        this.type = type;
        this.nextStateId = nextStateId;
        this.messageObj = messageObj;
        if (messageObj) {
            this.choices = messageObj.choices;
        }
        this.delayInMs = delayInMs;
    }

    static Wait(messageObj) {
        if (messageObj.type === MSG_TYPE.TEXT &&
            !messageObj.messages) {
            return undefined;
        }

        return new EventResult(EVENT_RESULT_TYPE.WAIT, {messageObj});
    }

    static SwitchState(nextStateId, delayInMs, messageObj) {
        if (!messageObj || (messageObj.type === MSG_TYPE.TEXT &&
            !messageObj.messages)) {
            messageObj = undefined;
        }
        return new EventResult(EVENT_RESULT_TYPE.SWITCH_STATE, {messageObj, nextStateId, delayInMs});
    }

    static ScenarioFinished(messageObj) {
        return new EventResult(EVENT_RESULT_TYPE.SCENARIO_FINISHED, {messageObj})
    }
}

module.exports = {EventResult, EVENT_RESULT_TYPE};
