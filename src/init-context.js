const log = require('./log/winston').log(__filename);

const initPlatformQueries = require('./platform/components/init-firebase-queries');
const initProjectQueries = require('./project_name/components/init/firebase-queries');
const initPlatformComponents = require('./platform/components/init/components');
const initProjectComponents = require('./project_name/components/init/components');

module.exports = function () {
    return new Promise(async (resolve) => {
        log.info('* initializing context...');

        const queries = initPlatformQueries();
        initProjectQueries(queries);

        const components = await initPlatformComponents(queries);
        initProjectComponents(components, queries);

        log.info('* context initialized...\n');

        return resolve(components);
    });
};
