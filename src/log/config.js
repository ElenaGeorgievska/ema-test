const fs = require('fs');
const winston = require('winston');

require('winston-daily-rotate-file');
const SlackHook = require("winston-slack-webhook-transport");

const {
    format,
    createLogger
} = winston;

const {
    combine,
    timestamp,
    printf
} = format;

const env = process.env.NODE_ENV || 'development';

const directory = 'logs';

if (!fs.existsSync(directory)) {
    fs.mkdirSync(directory);
}

const transports = {
    Console: new winston.transports.Console({
        level: 'debug'
    }),
    File: new winston.transports.DailyRotateFile({
        timestamp,
        level: 'debug',
        filename: `${directory}/-results.log`,
        datePattern: 'YYYY-MM-DD',
        prepend: true
    }),
    Slack: new (require('winston-slack-webhook-transport'))({
        webhookUrl: process.env.SLACK_LOGGING_WEBHOOK,
        level: 'error'
    })
};
const logger = createLogger({
    levels: {
        error: 0,
        warn: 1,
        info: 2,
        debug: 3
    }
});

logger.add(transports.Console);

if (process.env.SLACK_LOGGING_WEBHOOK) {
    logger.add(transports.Slack);
}

if (env === 'development') {
    logger.add(transports.File);
}

logger.format = combine(
    format.splat(),
    format.colorize(),
    timestamp(),
    printf(info =>
        `${(new Date()).toLocaleString()} | ${info.level} | ${info.message}`)
);

if (env === 'production' || env === 'staging') {
    process.on('uncaughtException', err => {
        return logger.error(`UncaughtException ${err}`);
    });
}


module.exports = logger;
