const logger = require('./config');
const utils = require('./utils');

module.exports = {
    log: caller => {
        return {
            info: message => logger.info(`[${utils.getFilePath(caller)}] | ${message}`),
            error: message => logger.error(`[${utils.getFilePath(caller)}] | ${message}`),
            warn: message => logger.warn(`[${utils.getFilePath(caller)}] | ${message}`),
            debug: message => {
                if (process.env.DEBUG_MODE) {
                    return logger.debug(`[${utils.getFilePath(caller)}] | ${message}`)
                }
            }
        }
    }
};
