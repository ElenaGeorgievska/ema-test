module.exports = {
    getFilePath: function (caller) {
        if (!caller || caller.length === 0) {
            return 'unknown file';
        }

        let paths;
        let length;

        if (caller.includes('/')) {
            paths = caller.split('/');
            length = paths.length - 1;
        } else if (caller.includes('\\')) {
            paths = caller.split('\\');
            length = paths.length - 1;
        }

        return paths[length - 1] + '/' + paths[length];
    }
};