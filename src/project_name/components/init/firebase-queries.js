const log = require('../../../log/winston').log(__filename);

const DbReferences = require('../repos/firebase/references');

const CustomQueries = require('../repos/firebase/queries/custom-queries');

module.exports = function (queries) {
    log.info(`* initializing the project firebase queries...`);

    const dbReferences = new DbReferences();

    queries.customQueries = new CustomQueries(dbReferences);

    log.info(`* project firebase queries initialized\n`);
    return queries;
};
