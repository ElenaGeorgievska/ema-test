const log = require('../../../log/winston').log(__filename);

const CustomService = require('../services/custom-service');

module.exports = function (components, {customQueries}) {
    log.info(`project default services`);

    components.customService = new CustomService(customQueries);
};
