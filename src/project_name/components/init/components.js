const GetProjectStateService = require('../states/project-state-provider');
const initProjectServices = require('../init/project-services');

module.exports = function (components, queries) {
    const getProjectStateService = new GetProjectStateService(components);
    components.getStateService.addProviders(getProjectStateService);

    initProjectServices(components, queries);
}
