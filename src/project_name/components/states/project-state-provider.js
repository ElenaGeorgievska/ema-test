const projectStateTypes = require('./project-state-types');
const CoffeeState = require('./impl/coffee-state');
const CarouselState = require('./impl/carousel-state');
const SwitchState = require('../../../platform/components/states/switch-state');

class ProjectStateProvider {
    constructor(components) {
        this.components = components;
    }

    getState(stateData) {
        switch (stateData.type) {
            case projectStateTypes.coffee:
                return new CoffeeState(stateData, this.components);
            case projectStateTypes.carousel:
                return new CarouselState(stateData, this.components);
            case projectStateTypes.tea:
                return new SwitchState(stateData, this.components);
        }
        return undefined;
    }
}

module.exports = ProjectStateProvider;
