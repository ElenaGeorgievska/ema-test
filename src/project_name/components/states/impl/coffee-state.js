const State = require('../../../../platform/components/states/state');
const {EventResult} = require('../../../../platform/models/event-result');
const {Message} = require('../../../../platform/models/message')

class CoffeeState extends State {
    constructor(stateData, {classifyApiService}) {
        super(stateData, {classifyApiService});
    }

    async onStart(event) {
        const messageObj = Message.TextMessages(['howdy, I like coffee'], this.choices);
        return EventResult.Wait(messageObj);
    }
}

module.exports = CoffeeState;
