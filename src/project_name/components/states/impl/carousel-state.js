const log = require('../../../../log/winston').log(__filename);

const State = require('../../../../platform/components/states/state');
const {EventResult} = require('../../../../platform/models/event-result');
const {Message} = require('../../../../platform/models/message');

const {getFirstChoice} = require('../../../../platform/components/states/common-functions');

class CarouselState extends State {
    constructor(stateData, {classifyApiService}) {
        super(stateData, {classifyApiService});
    }

    async onEvent(event) {
        log.debug(`received an event in the carousel state: ${JSON.stringify(event)}`);
        const selectedLink = event.link;
        const selectedValue = event.value;
        log.debug(`this is the selected item's link ${selectedLink}`);
        log.debug(`this is the selected item's value ${selectedValue}`);

        return super.onEvent(event);
    }

    async onStart(event) {
        const choice = getFirstChoice(this.choices);
        const elements = getFormattedElements(choice);

        const messageObj = Message.CarouselElements(elements, this.choices);
        return EventResult.Wait(messageObj);
    }
}

const getFormattedElements = function (choice) {
    const elements = [];
    for (let i = 0; i < 3; i++) {
        elements.push({
            title: `Title ${i}`,
            image_url: '',
            subtitle: `Description ${i}`,
            buttons: [
                {
                    type: 'postback',
                    title: `Title ${i}`,
                    payload: JSON.stringify({link: choice.link, value: `selected_value_${i}`})
                }
            ]
        });
    }
    return elements;
};


module.exports = CarouselState;
