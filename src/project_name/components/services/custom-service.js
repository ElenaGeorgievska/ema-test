class CustomService {
    constructor(customQueries) {
        this.customQueries = customQueries;
    }

    async doSomething(userId, data) {
        await this.customQueries.doSomething(userId, data)
    }
}

module.exports = CustomService;
