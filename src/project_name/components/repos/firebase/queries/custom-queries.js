class CustomQueries {
    constructor(dbReferences) {
        this.dbReferences = dbReferences;
    }

    async doSomething(userId, data) {
        await this.dbReferences.CUSTOM_REFERENCE.child(userId).update(data);
    }
}

module.exports = CustomQueries;
