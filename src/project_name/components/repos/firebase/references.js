/* Here you can find and add the database (Firebase) references */

const root = require('../../../../platform/components/repos/firebase/config');

class DbReferences {
    constructor() {
        this.PROJECT_RELATED_STUFF = root.child('project_related_stuff');
        this.CUSTOM_REFERENCE = this.PROJECT_RELATED_STUFF.child('custom');
    }
}

module.exports = DbReferences;
