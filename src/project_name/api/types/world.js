const {gql} = require("apollo-server-express");

const World = gql`
    "A type that contains a msg string"
    type World {
        msg: String!
    }
`;

module.exports = {World};
