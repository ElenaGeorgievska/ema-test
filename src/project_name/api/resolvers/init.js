const getQueryResolvers = require('./query');

const getResolvers = (components) => {
    return {
        Query: getQueryResolvers(components),
    }
};
module.exports = getResolvers;
