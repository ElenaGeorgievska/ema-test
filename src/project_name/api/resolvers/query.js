const log = require('../../../log/winston').log(__filename);

const getQueryResolvers = (components) => {
    return ({
        hello: async (root, {name}, context) => {
            if (!components) {
                log.warn('components are undefined!!!');
            }
            log.info(`Hello from ${name}`);
            return {msg: `Hello ${name} this is World`};
        },
    })
};
module.exports = getQueryResolvers;
