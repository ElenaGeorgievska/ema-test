const graphqlSchema = require('./schema');
const initResolvers = require('./resolvers/init');
const initProjectApi = (components) => {
    return {
        graphqlResolvers: initResolvers(components),
        graphqlSchema,
    }
}

module.exports = initProjectApi;
