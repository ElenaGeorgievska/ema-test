const {gql} = require("apollo-server-express");

const {World} = require('./types/world');

// A schema is a collection of type definitions (hence "typeDefs")
// that together define the "shape" of queries that are executed against
// your data.
// !!NOTE!! If defining Query, Mutation and Subscription
// make sure they use "extend" because we extend a Base GrapqQL schema
// to allow for composition of the API
const schema = gql`

    # Input and output types
    ${World}

    extend type Query {
        """
        Get a greeting from the server
        """
        hello(
            "Who should the server greet"
            name: String!
        ): World!
    }

`;
module.exports = schema;
