const log = require('./log/winston').log(__filename);
const express = require('express');
const bodyParser = require('body-parser');

const initGraphQL = require('./graphql/init-graphql');

const HTTP_STATUS_OK = 200;

module.exports = function (components, platformAPIs, projectAPIs) {
    const jsonParser = bodyParser.json();

    const app = express();
    const port = process.env.PORT || 4000;
    const env = process.env.NODE_ENV || 'development';

    app.use((req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT');
        res.header('Access-Control-Allow-Headers',
            'Origin, X-Requested-With, Content-Type, Accept, Authorization');
        next();
    });

    app.get('/webhook', (req, res) => {
        log.debug(`received verify webhook event`);
        return platformAPIs.messenger.eventHandler.onVerifyWebhookEvent(req, res);
    });

    app.post('/webhook', jsonParser, (req, res) => {
        log.debug(`received messenger event: [${JSON.stringify(req.body)}]`);

        res.sendStatus(HTTP_STATUS_OK);

        return platformAPIs.messenger.eventHandler.onMessageEvent(req.body);
    });

    app.post('/web', jsonParser, (req, res) => {
        log.debug(`received web event: [${JSON.stringify(req.body)}]`);
        res.sendStatus(HTTP_STATUS_OK);
        return platformAPIs.web.eventHandler.onMessageEvent(req.body);
    });

    const httpServer = initGraphQL(
        app,
        components,
        {schema: platformAPIs.web.graphqlSchema, resolvers: platformAPIs.web.graphqlResolvers},
        {schema: projectAPIs.graphqlSchema, resolvers: projectAPIs.graphqlResolvers}
    );

    const listener = httpServer.listen(port, () => {
        log.info('configuring server...');
        log.info(`running on environment: ** ${env} **`);
        log.info(`server started listening on port ${listener.address().port}\n`);

        if (process.env.VIBER_WEBHOOK) {
            startViber();
        }
    });

    function startViber() {
        const {Bot, Events} = require('viber-bot');

        const viberBot = new Bot({
            authToken: process.env.VIBER_ACCESS_TOKEN,
            name: process.env.BOT_NAME,
            avatar: process.env.VIBER_BOT_AVATAR
        });

        app.use('/viber/webhook', viberBot.middleware());

        viberBot.on(Events.MESSAGE_RECEIVED, (message, response) => {
            platformAPIs.viber.eventHandler.onMessageEvent(message, {
                userProfile: response.userProfile,
                viberBot
            });
            response.send();
        });

        viberBot.setWebhook(`${process.env.VIBER_WEBHOOK}/viber/webhook`)
            .then(() => log.info(`the viber webhook set successfully!`))
            .catch((e) => log.error(`error while setting the viber webhook: ${JSON.stringify(e)}!`));
    }
};


