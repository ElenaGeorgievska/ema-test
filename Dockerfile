FROM node:12
ADD package.json /backend/
WORKDIR /backend/
RUN npm install
ADD . /backend/
CMD ["node", "src/index.js"]
